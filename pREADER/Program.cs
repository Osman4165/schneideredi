﻿using DAL;
using EdiFabric.Core.Model.Edi;
using EdiFabric.Core.Model.Edi.X12;
using EdiFabric.Framework.Readers;
using EdiFabric.Templates.X12004010;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;
using EdiFabric.TranslateX12.Net45;

namespace EdiFabric.Translate.X12.Net45
{
    class Program
    {

        /// </summary>
        [STAThread]
        static void Main()
        {
            SerialKey.Set("BQHL4-XCBVH-LUFAS-NLIMC-D44GQ-ZNED2-EGSZD-ETJO5-CUKLU-YJT4C-AFKMR-CED");

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Execute());
        }

       
    }
}