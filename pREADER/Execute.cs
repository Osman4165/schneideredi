﻿using EdiFabric.Core.Model.Edi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DAL;
using EdiFabric.Core.Model.Edi.X12;
using EdiFabric.Framework.Readers;
using EdiFabric.Templates.X12004010;
using System.Configuration;

namespace EdiFabric.TranslateX12.Net45
{
    public partial class Execute : Form
    {
        System.Timers.Timer t;
         bool timer_enter;
        string c_str;
        public Execute()
        {
            InitializeComponent();
        }
    private void Execute_Load(object sender, EventArgs e)
        {
            timer_enter = false;
            c_str = ConfigurationManager.AppSettings["con_str"];
        }
        private void T_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                Label2.BeginInvoke((new Action(() => { Label2.Text = ("Started."); })));
                if (!timer_enter)
                {
                    timer_enter = true;
                    ReadEDI();
                    timer_enter = false;
                }
            }
            catch(Exception x)
            {
                Label2.BeginInvoke((new Action(() => { Label2.Text = ("Error :"+x.ToString()); })));


            }
        }
            private void ReadEDI()
        {
            t.Stop();
            string sOutboundFolder = ConfigurationManager.AppSettings["inboundDirectory"];
            List<IEdiItem> ediItems;
            string[] sEdiPathFiles = Directory.GetFiles(sOutboundFolder);

            if (sEdiPathFiles.Length == 0)
            {

            }
            else
            {
                Database d = new Database(DBEngineType.MsSql);
                d.con_string =c_str;
                d.cmd_type = CommandType.StoredProcedure;

                foreach (string sEdiPathFile in sEdiPathFiles)
                {
                    string str = File.ReadAllText(sEdiPathFile);

                    String format = "";
                    using (var reader = new X12Reader(File.OpenRead(sEdiPathFile), "EdiFabric.Templates.X12", new X12ReaderSettings { ContinueOnError = true }))

                        ediItems = reader.ReadToEnd().ToList();

                    var sEdiFile = Path.GetFileName(sEdiPathFile);

                    if (sEdiPathFiles.Length == 0)
                    {
                        Label2.BeginInvoke((new Action(() => { Label2.Text = ("There are no files in the EDI_Outbound folder."); })));
                    }
                    else
                    {
                        Label2.BeginInvoke((new Action(() => { Label2.Text = ("File is " + sEdiFile); })));
                    }
                    System.Threading.Thread.Sleep(1);
                    int _index = 0;
                    var s846 = ediItems.OfType<TS846>(); //Stok sorgulama
                    var s850 = ediItems.OfType<TS850>(); //Satınalma Siparişi
                    var s856 = ediItems.OfType<TS856>(); //Gönderi Bildirimi
                    var s888 = ediItems.OfType<TS888>(); //Gönderi Bakım İşlemi
                    var s944 = ediItems.OfType<TS944>(); //Depo Stok Transferi Fişi Tavsiyesi 
                    var s947 = ediItems.OfType<TS947>(); //Depo Envanter Ayarlama Önerisi - Rezervasyon
                    Type type = typeof(TS850);
                    if (s888 != null && s888.Count() > 0)
                    {
                        str = File.ReadAllText(sEdiPathFile);
                        if (str.IndexOf("~LX*") > 10)
                        {
                            str = str.Replace("~LX*", "~NTE*");
                            str = str.Replace("~PID*", "~H1*");
                            File.WriteAllText(sEdiPathFile, str);
                        }

                        using (var reader = new X12Reader(File.OpenRead(sEdiPathFile), "EdiFabric.Templates.X12", new X12ReaderSettings { ContinueOnError = true }))

                            ediItems = reader.ReadToEnd().ToList();
                        s888 = ediItems.OfType<TS888>();
                    }

                    if (s846 != null && s846.Count() > 0)
                    {
                        format = "EDI 846";
                        Label2.BeginInvoke((new Action(() => { Label2.Text = ("EDI X12 format is " + format); })));
                        int MainID = 0;
                        foreach (TS846 result in s846)
                        {
                            _index = 0;
                            try
                            {

                                d = new Database(DBEngineType.MsSql);
                                d.con_string =c_str;
                                d.cmd_type = CommandType.StoredProcedure;
                                //  Indicates the start of a purchase order transaction set and assigns a control number.

                                var TransactionSetPurposeCode_01 = result.BIA.TransactionSetPurposeCode_01;
                                var ReportTypeCode_02 = result.BIA.ReportTypeCode_02;
                                var ReferenceIdentification_03 = result.BIA.ReferenceIdentification_03;// = "00";
                                var Date_04 = result.BIA.Date_04;// = "SA";
                                var Time_05 = result.BIA.Time_05;//= "XX-1234";

                                d.text = "pT846H";
                                d.add_parameter("TransactionSetPurposeCode_01", TransactionSetPurposeCode_01 == null ? "" : TransactionSetPurposeCode_01);
                                d.add_parameter("ReportTypeCode_02", ReportTypeCode_02 == null ? "" : ReportTypeCode_02);
                                d.add_parameter("ReferenceIdentification_03", ReferenceIdentification_03 == null ? "" : ReferenceIdentification_03);
                                d.add_parameter("Date_04", Date_04);
                                d.add_parameter("Time_05", Time_05);
                                d.add_parameter("@In2Out", 0);
                                d.add_parameter("@Out2In", 1);
                                MainID = Convert.ToInt32(d.exec_salar());
                                //  REF
                                _index = 0;
                                foreach (var rf in result.REF)
                                {
                                    d = new Database(DBEngineType.MsSql);
                                    d.con_string =c_str;
                                    d.cmd_type = CommandType.StoredProcedure;
                                    var ReferenceIdentificationQualifier_01 = rf.ReferenceIdentificationQualifier_01;
                                    var ReferenceIdentification_02 = rf.ReferenceIdentification_02;
                                    d.text = "pT846H_ref";
                                    d.add_parameter("MainID", MainID);
                                    d.add_parameter("ReferenceIdentificationQualifier_01", ReferenceIdentificationQualifier_01 == null ? "" : ReferenceIdentificationQualifier_01);
                                    d.add_parameter("ReferenceIdentification_02", ReferenceIdentification_02 == null ? "" : ReferenceIdentification_02);
                                    d.add_parameter("_index", _index++);
                                    d.exec_non();
                                    _index++;
                                }

                                _index = 0;
                                //  N1 N2
                                if (result.N1Loop != null)
                                    foreach (var n in result.N1Loop)
                                    {
                                        d = new Database(DBEngineType.MsSql);
                                        d.con_string =c_str;
                                        d.cmd_type = CommandType.StoredProcedure;
                                        var EntityIdentifierCode_01 = n.N1.EntityIdentifierCode_01;
                                        var N1_Name_02 = n.N1.Name_02;
                                        var IdentificationCodeQualifier_03 = n.N1.IdentificationCodeQualifier_03;
                                        var IdentificationCode_04 = n.N1.IdentificationCode_04;
                                        d.text = "pT846H_n1";
                                        d.add_parameter("MainID", MainID);
                                        d.add_parameter("EntityIdentifierCode_01", EntityIdentifierCode_01 == null ? "" : EntityIdentifierCode_01);
                                        d.add_parameter("Name_02", N1_Name_02 == null ? "" : N1_Name_02);
                                        d.add_parameter("IdentificationCodeQualifier_03", IdentificationCodeQualifier_03 == null ? "" : IdentificationCodeQualifier_03);
                                        d.add_parameter("IdentificationCode_04", IdentificationCode_04 == null ? "" : IdentificationCode_04);
                                        d.add_parameter("_index", _index++);

                                        d.exec_non();


                                    }

                                _index = 0;
                                //      PO1
                                foreach (var lin in result.LINLoop)
                                {
                                    // PO PO1
                                    d = new Database(DBEngineType.MsSql);
                                    d.con_string =c_str;
                                    d.cmd_type = CommandType.StoredProcedure;

                                    var ProductServiceIDQualifier_02 = lin.LIN.ProductServiceIDQualifier_02 == null ? "" : lin.LIN.ProductServiceIDQualifier_02;
                                    var ProductServiceID_03 = lin.LIN.ProductServiceID_03 == null ? "" : lin.LIN.ProductServiceID_03;
                                    var ProductServiceIDQualifier_04 = lin.LIN.ProductServiceIDQualifier_04 == null ? "" : lin.LIN.ProductServiceIDQualifier_04;
                                    var ProductServiceID_05 = lin.LIN.ProductServiceID_05 == null ? "" : lin.LIN.ProductServiceID_05;
                                    var ProductServiceIDQualifier_06 = lin.LIN.ProductServiceIDQualifier_06 == null ? "" : lin.LIN.ProductServiceIDQualifier_06;
                                    var ProductServiceID_07 = lin.LIN.ProductServiceID_07 == null ? "" : lin.LIN.ProductServiceID_07;
                                    d.text = "pT846H_lin";
                                    d.add_parameter("MainID", MainID);
                                    d.add_parameter("ProductServiceIDQualifier_02", ProductServiceIDQualifier_02 == null ? "" : ProductServiceIDQualifier_02);
                                    d.add_parameter("ProductServiceID_03", ProductServiceID_03 == null ? "" : ProductServiceID_03);
                                    d.add_parameter("ProductServiceIDQualifier_04", ProductServiceIDQualifier_04 == null ? "" : ProductServiceIDQualifier_04);
                                    d.add_parameter("ProductServiceID_05", ProductServiceID_05 == null ? "" : ProductServiceID_05);
                                    d.add_parameter("ProductServiceIDQualifier_06", ProductServiceIDQualifier_06 == null ? "" : ProductServiceIDQualifier_06);
                                    d.add_parameter("ProductServiceID_07", ProductServiceID_07 == null ? "" : ProductServiceID_07);
                                    d.add_parameter("_index", _index++);

                                    int LinID = Convert.ToInt32(d.exec_salar());
                                    int _index_qty = 0;
                                    // QTY
                                    foreach (var qty in lin.QTYLoop)
                                    {
                                        d = new Database(DBEngineType.MsSql);
                                        d.con_string =c_str;
                                        d.cmd_type = CommandType.StoredProcedure;
                                        var QuantityQualifier_01 = qty.QTY.QuantityQualifier_01;
                                        var Quantity_02 = qty.QTY.Quantity_02;
                                        d.text = "pT846H_lin_qty";
                                        d.add_parameter("MainID", MainID);
                                        d.add_parameter("LinID", LinID);
                                        d.add_parameter("QuantityQualifier_01", QuantityQualifier_01 == null ? "" : QuantityQualifier_01);
                                        d.add_parameter("Quantity_02", Quantity_02 == null ? "" : Quantity_02);
                                        d.add_parameter("_index", _index_qty++);

                                        d.exec_non();

                                        d = new Database(DBEngineType.MsSql);
                                        d.con_string =c_str;
                                        d.cmd_type = CommandType.StoredProcedure;
                                        int _index_uit = 0;

                                        // UIT
                                        if (qty.UIT != null)
                                            foreach (var uit in qty.UIT)
                                            {

                                                var UnitorBasisforMeasurementCode_01 = uit.CompositeUnitofMeasure_01.UnitorBasisforMeasurementCode_01;

                                                d.text = "pT846H_lin_uit";
                                                d.add_parameter("MainID", MainID);
                                                d.add_parameter("LinID", LinID);
                                                d.add_parameter("UnitorBasisforMeasurementCode_01", UnitorBasisforMeasurementCode_01 == null ? "" : UnitorBasisforMeasurementCode_01.ToString());
                                                d.add_parameter("_index", _index_uit++);

                                                d.exec_non();
                                            } }



                                }
                                if (result.HasErrors)
                                {
                                    Label2.BeginInvoke((new Action(() => { Label2.Text = ("Rollback format is " + format); })));
                                    d = new Database(DBEngineType.MsSql);
                                    d.con_string =c_str;
                                    d.cmd_type = CommandType.StoredProcedure;
                                    d.text = "delT846H";
                                    d.add_parameter("MainID", MainID);
                                    d.add_parameter("Fname", sEdiFile);

                                    d.exec_non();
                                    if (File.Exists(sOutboundFolder + sEdiFile))
                                    {
                                        File.Move(sOutboundFolder + sEdiFile, sOutboundFolder + "\\Error\\" + sEdiFile);
                                    }
                                }
                                else
                                {

                                    d = new Database(DBEngineType.MsSql);
                                    d.con_string =c_str;
                                    d.cmd_type = CommandType.StoredProcedure;
                                    d.text = "up846";
                                    d.add_parameter("ID", MainID);
                                    d.exec_non();
                                    if (File.Exists(sOutboundFolder + sEdiFile))
                                    {
                                        File.Move(sOutboundFolder + sEdiFile, sOutboundFolder + "\\Success\\" + sEdiFile);
                                    }
                                }
                            }
                            catch (Exception x)
                            {

                                Label2.BeginInvoke((new Action(() => { Label2.Text = ("Rollback format is " + format); })));
                                d = new Database(DBEngineType.MsSql);
                                d.con_string =c_str;
                                d.cmd_type = CommandType.StoredProcedure;
                                d.text = "delT846H";
                                d.add_parameter("Msj", x.Message);
                                d.add_parameter("MainID", MainID);
                                d.add_parameter("Fname", sEdiFile);

                                d.exec_non();
                                if (File.Exists(sOutboundFolder + sEdiFile))
                                {
                                    File.Move(sOutboundFolder + sEdiFile, sOutboundFolder + "\\Error\\" + sEdiFile);
                                }
                            }


                        }

                    }
                    if (s850 != null && s850.Count() > 0)
                    {
                        format = "EDI 850";
                        Label2.BeginInvoke((new Action(() => { Label2.Text = ("EDI X12 format is " + format); })));
                        int MainID = 0;
                        foreach (TS850 result in s850)
                        {
                            _index = 0;
                            //  Indicates the start of a purchase order transaction set and assigns a control number.
                            try {

                                var TransactionSetPurposeCode_01 = result.BEG.TransactionSetPurposeCode_01;// = "00";
                                var PurchaseOrderTypeCode_02 = result.BEG.PurchaseOrderTypeCode_02;// = "SA";
                                var PurchaseOrderNumber_03 = result.BEG.PurchaseOrderNumber_03;//= "XX-1234";
                                var Date_05 = result.BEG.Date_05;// = "20170301";
                                                                 //  result.BEG.AcknowledgmentType_07 = "NA";


                                d = new Database(DBEngineType.MsSql);
                                d.con_string =c_str;
                                d.cmd_type = CommandType.StoredProcedure;
                                d.text = "pT850H";
                                d.add_parameter("TransactionSetPurposeCode_01", TransactionSetPurposeCode_01 == null ? "" : TransactionSetPurposeCode_01);
                                d.add_parameter("PurchaseOrderTypeCode_02", PurchaseOrderTypeCode_02 == null ? "" : PurchaseOrderTypeCode_02);
                                d.add_parameter("PurchaseOrderNumber_03", PurchaseOrderNumber_03 == null ? "" : PurchaseOrderNumber_03);
                                d.add_parameter("Date_05", Date_05 == null ? "" : Date_05);
                                d.add_parameter("@In2Out", 0);
                                d.add_parameter("@Out2In", 1);
                                MainID = Convert.ToInt32(d.exec_salar());
                                _index = 0;
                                //  REF
                                foreach (var rf in result.REF)
                                {
                                    d = new Database(DBEngineType.MsSql);
                                    d.con_string =c_str;
                                    d.cmd_type = CommandType.StoredProcedure;
                                    var ReferenceIdentificationQualifier_01 = rf.ReferenceIdentificationQualifier_01;
                                    var ReferenceIdentification_02 = rf.ReferenceIdentification_02;

                                    d.text = "pT850H_ref";
                                    d.add_parameter("ReferenceIdentificationQualifier_01", ReferenceIdentificationQualifier_01 == null ? "" : ReferenceIdentificationQualifier_01);
                                    d.add_parameter("ReferenceIdentification_02", ReferenceIdentification_02 == null ? "" : ReferenceIdentification_02);
                                    d.add_parameter("MainID", MainID);
                                    d.add_parameter("_index", _index++);
                                    d.exec_non();
                                }

                                _index = 0;
                                //FOB
                                foreach (var fob in result.FOB)
                                {
                                    d = new Database(DBEngineType.MsSql);
                                    d.con_string =c_str;
                                    d.cmd_type = CommandType.StoredProcedure;
                                    var ShipmentMethodofPayment_01 = fob.ShipmentMethodofPayment_01;
                                    var TransportationTermsQualifierCode_04 = fob.TransportationTermsQualifierCode_04;
                                    var TransportationTermsCode_05 = fob.TransportationTermsCode_05;
                                    var LocationQualifier_06 = fob.LocationQualifier_06;
                                    var Description_07 = fob.Description_07;
                                    d.text = "pT850H_fob";
                                    d.add_parameter("ShipmentMethodofPayment_01", ShipmentMethodofPayment_01 == null ? "" : ShipmentMethodofPayment_01);
                                    d.add_parameter("TransportationTermsQualifierCode_04", TransportationTermsQualifierCode_04 == null ? "" : TransportationTermsQualifierCode_04);
                                    d.add_parameter("TransportationTermsCode_05", TransportationTermsCode_05 == null ? "" : TransportationTermsCode_05);
                                    d.add_parameter("LocationQualifier_06", LocationQualifier_06 == null ? "" : LocationQualifier_06);
                                    d.add_parameter("Description_07", Description_07 == null ? "" : Description_07);
                                    d.add_parameter("MainID", MainID);
                                    d.add_parameter("_index", _index++);

                                    d.exec_non();
                                }
                                _index = 0;         //DTM
                                if (result.DTM != null)
                                    foreach (var dtm in result.DTM)
                                {
                                    d = new Database(DBEngineType.MsSql);
                                    d.con_string =c_str;
                                    d.cmd_type = CommandType.StoredProcedure;
                                    var DateTimeQualifier_01 = dtm.DateTimeQualifier_01;
                                    var Date_02 = dtm.Date_02;
                                    var Time_03 = dtm.Time_03;
                                    var TimeCode_04 = dtm.TimeCode_04;
                                    d.text = "pT850H_dtm";
                                    d.add_parameter("DateTimeQualifier_01", DateTimeQualifier_01 == null ? "" : DateTimeQualifier_01);
                                    d.add_parameter("Date_02", Date_02 == null ? "" : Date_02);
                                    d.add_parameter("Time_03", Time_03 == null ? "" : Time_03);
                                    d.add_parameter("TimeCode_04", TimeCode_04 == null ? "" : TimeCode_04);
                                    d.add_parameter("MainID", MainID);
                                    d.add_parameter("_index", _index++);
                                    d.exec_non();

                                }
                                _index = 0;      //TD5
                                if(result.TD5!=null)
                                foreach (var td5 in result.TD5)
                                {
                                    d = new Database(DBEngineType.MsSql);
                                    d.con_string =c_str;
                                    d.cmd_type = CommandType.StoredProcedure;
                                    var IdentificationCodeQualifier_02 = td5.IdentificationCodeQualifier_02;
                                    var IdentificationCode_03 = td5.IdentificationCode_03;
                                    var TransportationMethodTypeCode_04 = td5.TransportationMethodTypeCode_04;
                                    var Routing_05 = td5.Routing_05;
                                    var ServiceLevelCode_12 = td5.ServiceLevelCode_12;

                                    d.text = "pT850H_td5";
                                    d.add_parameter("MainID", MainID);
                                    d.add_parameter("IdentificationCodeQualifier_02", IdentificationCodeQualifier_02 == null ? "" : IdentificationCodeQualifier_02);
                                    d.add_parameter("IdentificationCode_03", IdentificationCode_03 == null ? "" : IdentificationCode_03);
                                    d.add_parameter("TransportationMethodTypeCode_04", TransportationMethodTypeCode_04 == null ? "" : TransportationMethodTypeCode_04);
                                    d.add_parameter("Routing_05", Routing_05 == null ? "" : Routing_05);
                                    d.add_parameter("ServiceLevelCode_12", ServiceLevelCode_12 == null ? "" : ServiceLevelCode_12);
                                    d.add_parameter("_index", _index++);

                                    d.exec_non();

                                }
                                int __index = 0;
                                //N9
                                if (result.N9Loop != null)
                                    foreach (var n9 in result.N9Loop)
                                    {
                                        d = new Database(DBEngineType.MsSql);
                                        d.con_string =c_str;
                                        d.cmd_type = CommandType.StoredProcedure;
                                        var ReferenceIdentificationQualifier_01 = n9.N9.ReferenceIdentificationQualifier_01;
                                        var ReferenceIdentification_02 = n9.N9.ReferenceIdentification_02;
                                        d.text = "pT850H_n9";
                                        d.add_parameter("MainID", MainID);
                                        d.add_parameter("ReferenceIdentificationQualifier_01", ReferenceIdentificationQualifier_01 == null ? "" : ReferenceIdentificationQualifier_01);
                                        d.add_parameter("ReferenceIdentification_02", ReferenceIdentification_02 == null ? "" : ReferenceIdentification_02);
                                        d.add_parameter("_index", __index);

                                        int N9ID = Convert.ToInt32(d.exec_salar());
                                        
                                        foreach (var m in n9.MSG)
                                        {
                                            d = new Database(DBEngineType.MsSql);
                                            d.con_string =c_str;
                                            d.cmd_type = CommandType.StoredProcedure;
                                            var FreeFormMessageText_01 = m.FreeFormMessageText_01;
                                            var PrinterCarriageControlCode_02 = m.PrinterCarriageControlCode_02;
                                            var Number_03 = m.Number_03;
                                            d.text = "pT850H_n9_msg";
                                            d.add_parameter("MainID", MainID);
                                            d.add_parameter("N9ID", N9ID);
                                            d.add_parameter("FreeFormMessageText_01", FreeFormMessageText_01 == null ? "" : FreeFormMessageText_01);
                                            d.add_parameter("PrinterCarriageControlCode_02", PrinterCarriageControlCode_02 == null ? "" : PrinterCarriageControlCode_02);
                                            d.add_parameter("Number_03", Number_03 == null ? "" : Number_03);
                                            d.add_parameter("_index", __index);

                                            d.exec_non();
                                        }

                                        __index++;
                                    }

                                int i = 0;
                                int NID = 0;
                                if (result.N1Loop != null)
                                    foreach (var n in result.N1Loop)
                                    {
                                        if (i == 0)
                                        {

                                            d = new Database(DBEngineType.MsSql);
                                            d.con_string =c_str;
                                            d.cmd_type = CommandType.StoredProcedure;
                                            var EntityIdentifierCode_01 = n.N1.EntityIdentifierCode_01;
                                            var N1_Name_02 = n.N1.Name_02;
                                            var IdentificationCodeQualifier_03 = n.N1.IdentificationCodeQualifier_03;
                                            var IdentificationCode_04 = n.N1.IdentificationCode_04;
                                            d.text = "pT850H_n";
                                            d.add_parameter("MainID", MainID);
                                            d.add_parameter("EntityIdentifierCode_01", EntityIdentifierCode_01 == null ? "" : EntityIdentifierCode_01);
                                            d.add_parameter("Name_02", N1_Name_02 == null ? "" : N1_Name_02);
                                            d.add_parameter("IdentificationCodeQualifier_03", IdentificationCodeQualifier_03 == null ? "" : IdentificationCodeQualifier_03);
                                            d.add_parameter("IdentificationCode_04", IdentificationCode_04 == null ? "" : IdentificationCode_04);
                                            d.add_parameter("_index", i);

                                            NID = Convert.ToInt32(d.exec_salar());

                                        }
                                        else
                                        {
                                            d = new Database(DBEngineType.MsSql);
                                            d.con_string =c_str;
                                            d.cmd_type = CommandType.StoredProcedure;
                                            var EntityIdentifierCode_01 = n.N1.EntityIdentifierCode_01;
                                            var N1_Name_02 = n.N1.Name_02;
                                            var IdentificationCodeQualifier_03 = n.N1.IdentificationCodeQualifier_03;
                                            var IdentificationCode_04 = n.N1.IdentificationCode_04;
                                            d.text = "pT850H_n_n1";
                                            d.add_parameter("MainID", MainID);
                                            d.add_parameter("EntityIdentifierCode_01", EntityIdentifierCode_01 == null ? "" : EntityIdentifierCode_01);
                                            d.add_parameter("Name_02", N1_Name_02 == null ? "" : N1_Name_02);
                                            d.add_parameter("NID", NID);
                                            d.add_parameter("IdentificationCodeQualifier_03", IdentificationCodeQualifier_03 == null ? "" : IdentificationCodeQualifier_03);
                                            d.add_parameter("IdentificationCode_04", IdentificationCode_04 == null ? "" : IdentificationCode_04);
                                            d.add_parameter("_index", i);

                                            // int N1ID = Convert.ToInt32(d.exec_salar());
                                            d.exec_non();

                                            int n2_ind = 0;
                                            //  N2
                                            if (n.N2 != null)
                                                foreach (var n2 in n.N2)
                                                {
                                                    d = new Database(DBEngineType.MsSql);
                                                    d.con_string =c_str;
                                                    d.cmd_type = CommandType.StoredProcedure;

                                                    var Name_01 = n2.Name_01;
                                                    var Name_02 = n2.Name_02;
                                                    d.text = "pT850H_n_n2";
                                                    d.add_parameter("MainID", MainID);
                                                    d.add_parameter("N1ID", NID);
                                                    d.add_parameter("Name_01", Name_01 == null ? "" : Name_01);
                                                    d.add_parameter("Name_02", Name_02 == null ? "" : Name_02);
                                                    d.add_parameter("_index", i);


                                                    d.exec_non();

                                                } //  N3
                                            int n3_ind = 0;

                                            if (n.N3 != null)
                                                foreach (var n3 in n.N3)
                                                {
                                                    d = new Database(DBEngineType.MsSql);
                                                    d.con_string =c_str;
                                                    d.cmd_type = CommandType.StoredProcedure;
                                                    var AddressInformation_01 = n3.AddressInformation_01;
                                                    var AddressInformation_02 = n3.AddressInformation_02;
                                                    d.text = "pT850H_n_n3";
                                                    d.add_parameter("MainID", MainID);
                                                    d.add_parameter("N1ID", NID);
                                                    d.add_parameter("AddressInformation_01", AddressInformation_01 == null ? "" : AddressInformation_01);
                                                    d.add_parameter("AddressInformation_02", AddressInformation_02 == null ? "" : AddressInformation_02);
                                                    d.add_parameter("_index", i);


                                                    d.exec_non();

                                                }//  N4
                                            int n4_ind = 0;

                                            if (n.N4 != null)

                                                foreach (var n4 in n.N4)
                                                {
                                                    d = new Database(DBEngineType.MsSql);
                                                    d.con_string =c_str;
                                                    d.cmd_type = CommandType.StoredProcedure;
                                                    var CityName_01 = n4.CityName_01;
                                                    var StateorProvinceCode_02 = n4.StateorProvinceCode_02;
                                                    var PostalCode_03 = n4.PostalCode_03;
                                                    var CountryCode_04 = n4.CountryCode_04;

                                                    d.text = "pT850H_n_n4";
                                                    d.add_parameter("MainID", MainID);
                                                    d.add_parameter("N1ID", NID);
                                                    d.add_parameter("CityName_01", CityName_01 == null ? "" : CityName_01);
                                                    d.add_parameter("StateorProvinceCode_02", StateorProvinceCode_02 == null ? "" : StateorProvinceCode_02);
                                                    d.add_parameter("PostalCode_03", PostalCode_03 != null ? PostalCode_03 : "");
                                                    d.add_parameter("CountryCode_04", CountryCode_04 == null ? "" : CountryCode_04);
                                                    d.add_parameter("_index", i);


                                                    d.exec_non();

                                                }//  PER
                                            int per_ind = 0;

                                            if (n.PER != null)

                                                foreach (var per in n.PER)
                                                {
                                                    d = new Database(DBEngineType.MsSql);
                                                    d.con_string =c_str;
                                                    d.cmd_type = CommandType.StoredProcedure;
                                                    var ContactFunctionCode_01 = per.ContactFunctionCode_01;
                                                    var CommunicationNumberQualifier_03 = per.CommunicationNumberQualifier_03;
                                                    var CommunicationNumber_04 = per.CommunicationNumber_04;
                                                    var CommunicationNumberQualifier_05 = per.CommunicationNumberQualifier_05;
                                                    var CommunicationNumber_06 = per.CommunicationNumber_06;
                                                    d.text = "pT850H_n_per";
                                                    d.add_parameter("MainID", MainID);
                                                    d.add_parameter("N1ID", NID);
                                                    d.add_parameter("ContactFunctionCode_01", ContactFunctionCode_01 == null ? "" : ContactFunctionCode_01);
                                                    d.add_parameter("CommunicationNumberQualifier_03", CommunicationNumberQualifier_03 == null ? "" : CommunicationNumberQualifier_03);
                                                    d.add_parameter("CommunicationNumber_04", CommunicationNumber_04 == null ? "" : CommunicationNumber_04);
                                                    d.add_parameter("CommunicationNumberQualifier_05", CommunicationNumberQualifier_05 == null ? "" : CommunicationNumberQualifier_05);
                                                    d.add_parameter("CommunicationNumber_06", CommunicationNumber_06 == null ? "" : CommunicationNumber_06);
                                                    d.add_parameter("_index", i);


                                                    d.exec_non();

                                                }
                                        }


                                        i++;
                                    }

                                int po_index_850 = 0;

                                //      PO1
                                foreach (var po in result.PO1Loop)
                                {
                                    // PO PO1
                                    d = new Database(DBEngineType.MsSql);
                                    d.con_string =c_str;
                                    d.cmd_type = CommandType.StoredProcedure;
                                    var AssignedIdentification_01 = po.PO1.AssignedIdentification_01;
                                    var QuantityOrdered_02 = po.PO1.QuantityOrdered_02;
                                    var UnitorBasisforMeasurementCode_03 = po.PO1.UnitorBasisforMeasurementCode_03;
                                    var ProductServiceIDQualifier_06 = po.PO1.ProductServiceIDQualifier_06;
                                    var ProductServiceID_07 = po.PO1.ProductServiceID_07;
                                    var ProductServiceIDQualifier_08 = po.PO1.ProductServiceIDQualifier_08;
                                    var ProductServiceID_09 = po.PO1.ProductServiceID_09;
                                    var ProductServiceIDQualifier_10 = po.PO1.ProductServiceIDQualifier_10;
                                    var ProductServiceID_11 = po.PO1.ProductServiceID_11;
                                    var ProductServiceIDQualifier_12 = po.PO1.ProductServiceIDQualifier_12;
                                    var ProductServiceID_13 = po.PO1.ProductServiceID_13;
                                    var ProductServiceIDQualifier_14 = po.PO1.ProductServiceIDQualifier_14;
                                    var ProductServiceID_15 = po.PO1.ProductServiceID_15;
                                    var ProductServiceIDQualifier_16 = po.PO1.ProductServiceIDQualifier_16;
                                    var ProductServiceID_17 = po.PO1.ProductServiceID_17;
                                    var ProductServiceIDQualifier_18 = po.PO1.ProductServiceIDQualifier_18;
                                    var ProductServiceID_19 = po.PO1.ProductServiceID_19;
                                    var ProductServiceIDQualifier_20 = po.PO1.ProductServiceIDQualifier_20;
                                    var ProductServiceID_21 = po.PO1.ProductServiceID_21;

                                    d.text = "pT850H_po";
                                    d.add_parameter("MainID", MainID);
                                    d.add_parameter("AssignedIdentification_01", AssignedIdentification_01 == null ? "" : AssignedIdentification_01);
                                    d.add_parameter("QuantityOrdered_02", QuantityOrdered_02 == null ? "" : QuantityOrdered_02);
                                    d.add_parameter("UnitorBasisforMeasurementCode_03", UnitorBasisforMeasurementCode_03 == null ? "" : UnitorBasisforMeasurementCode_03);
                                    d.add_parameter("ProductServiceIDQualifier_06", ProductServiceIDQualifier_06 == null ? "" : ProductServiceIDQualifier_06);
                                    d.add_parameter("ProductServiceID_07", ProductServiceID_07 == null ? "" : ProductServiceID_07);
                                    d.add_parameter("ProductServiceIDQualifier_08", ProductServiceIDQualifier_08 == null ? "" : ProductServiceIDQualifier_08);
                                    d.add_parameter("ProductServiceID_09", ProductServiceID_09 == null ? "" : ProductServiceID_09);
                                    d.add_parameter("ProductServiceIDQualifier_10", ProductServiceIDQualifier_10 == null ? "" : ProductServiceIDQualifier_10);
                                    d.add_parameter("ProductServiceID_11", ProductServiceID_11 == null ? "" : ProductServiceID_11);
                                    d.add_parameter("ProductServiceIDQualifier_12", ProductServiceIDQualifier_12 == null ? "" : ProductServiceIDQualifier_12);
                                    d.add_parameter("ProductServiceID_13", ProductServiceID_13 == null ? "" : ProductServiceID_13);
                                    d.add_parameter("ProductServiceIDQualifier_14", ProductServiceIDQualifier_14 == null ? "" : ProductServiceIDQualifier_14);
                                    d.add_parameter("ProductServiceID_15", ProductServiceID_15 == null ? "" : ProductServiceID_15);
                                    d.add_parameter("ProductServiceIDQualifier_16", ProductServiceIDQualifier_16 == null ? "" : ProductServiceIDQualifier_16);
                                    d.add_parameter("ProductServiceID_17", ProductServiceID_17 == null ? "" : ProductServiceID_17);
                                    d.add_parameter("ProductServiceIDQualifier_18", ProductServiceIDQualifier_18 == null ? "" : ProductServiceIDQualifier_18);
                                    d.add_parameter("ProductServiceID_19", ProductServiceID_19 == null ? "" : ProductServiceID_19);
                                    d.add_parameter("ProductServiceIDQualifier_20", ProductServiceIDQualifier_20 == null ? "" : ProductServiceIDQualifier_20);
                                    d.add_parameter("_index", po_index_850++);
                                    d.add_parameter("ProductServiceID_21", ProductServiceID_21 == null ? "" : ProductServiceID_21);
                                    int POID = Convert.ToInt32(d.exec_salar());

                                    int _po_pid_index = 0;
                                    //  PO PID
                                    foreach (var rf in po.PIDLoop)
                                    {
                                        var ItemDescriptionType_01 = rf.PID.ItemDescriptionType_01;
                                        var Description_05 = rf.PID.Description_05;
                                        d = new Database(DBEngineType.MsSql);
                                        d.con_string =c_str;
                                        d.cmd_type = CommandType.StoredProcedure;
                                        d.text = "pT850H_po_pid";
                                        d.add_parameter("MainID", MainID);
                                        d.add_parameter("POID", POID);
                                        d.add_parameter("ItemDescriptionType_01", ItemDescriptionType_01 == null ? "" : ItemDescriptionType_01);
                                        d.add_parameter("Description_05", Description_05 == null ? "" : Description_05);
                                        d.add_parameter("_index", _po_pid_index++);

                                        d.exec_non();
                                    }
                                    int _po_ref = 0;

                                    //  PO REF
                                    if (po.REF!=null)
                                    foreach (var rf in po.REF)
                                    {
                                        d = new Database(DBEngineType.MsSql);
                                        d.con_string =c_str;
                                        d.cmd_type = CommandType.StoredProcedure;
                                        var ReferenceIdentificationQualifier_01 = rf.ReferenceIdentificationQualifier_01;
                                        var ReferenceIdentification_02 = rf.ReferenceIdentification_02;
                                        d.text = "pT850H_po_ref";
                                        d.add_parameter("MainID", MainID);
                                        d.add_parameter("POID", POID);
                                        d.add_parameter("ReferenceIdentificationQualifier_01", ReferenceIdentificationQualifier_01 == null ? "" : ReferenceIdentificationQualifier_01);
                                        d.add_parameter("ReferenceIdentification_02", ReferenceIdentification_02 == null ? "" : ReferenceIdentification_02);
                                        d.add_parameter("_index", _po_ref++);

                                        d.exec_non();

                                    }
                                    int po_n9 = 0;
                                    //  N9
                                    if (po.N9Loop != null)
                                        foreach (var n9 in po.N9Loop)
                                        {
                                            d = new Database(DBEngineType.MsSql);
                                            d.con_string =c_str;
                                            d.cmd_type = CommandType.StoredProcedure;
                                            var poReferenceIdentificationQualifier_01 = n9.N9.ReferenceIdentificationQualifier_01;
                                            var poReferenceIdentification_02 = n9.N9.ReferenceIdentification_02;

                                            d.text = "pT850H_po_n9";
                                            d.add_parameter("MainID", MainID);
                                            d.add_parameter("POID", POID);
                                            d.add_parameter("ReferenceIdentificationQualifier_01", poReferenceIdentificationQualifier_01 == null ? "" : poReferenceIdentificationQualifier_01);
                                            d.add_parameter("ReferenceIdentification_02", poReferenceIdentification_02 == null ? "" : poReferenceIdentification_02);
                                            d.add_parameter("_index", po_n9++);

                                            int N9ID = Convert.ToInt32(d.exec_salar());
                                            int po_n9_msg = 0;

                                            if (n9.MSG != null)
                                                foreach (var m in n9.MSG)
                                                {
                                                    d = new Database(DBEngineType.MsSql);
                                                    d.con_string =c_str;
                                                    d.cmd_type = CommandType.StoredProcedure;
                                                    var FreeFormMessageText_01 = m.FreeFormMessageText_01;
                                                    var PrinterCarriageControlCode_02 = m.PrinterCarriageControlCode_02;
                                                    var Number_03 = m.Number_03;
                                                    d.text = "pT850H_po_n9_msg";
                                                    d.add_parameter("MainID", MainID);
                                                    d.add_parameter("N9ID", N9ID);
                                                    d.add_parameter("FreeFormMessageText_01", FreeFormMessageText_01 == null ? "" : FreeFormMessageText_01);
                                                    d.add_parameter("PrinterCarriageControlCode_02", PrinterCarriageControlCode_02 == null ? "" : PrinterCarriageControlCode_02);
                                                    d.add_parameter("Number_03", Number_03 == null ? "" : Number_03);
                                                    d.add_parameter("_index", po_n9_msg++);

                                                    d.exec_non();
                                                }
                                        }
                                }




                                if (result.HasErrors)
                                {
                                    Label2.BeginInvoke((new Action(() => { Label2.Text = ("Rollback format is " + format); })));
                                    d = new Database(DBEngineType.MsSql);
                                    d.con_string =c_str;
                                    d.cmd_type = CommandType.StoredProcedure;
                                    d.text = "delT850H";
                                    d.add_parameter("MainID", MainID);
                                    d.add_parameter("Fname", sEdiFile);
                                    d.exec_non();
                                    if (File.Exists(sOutboundFolder + sEdiFile))
                                    {
                                        File.Move(sOutboundFolder + sEdiFile, sOutboundFolder + "\\Error\\" + sEdiFile);
                                    }
                                }
                                else
                                {

                                    d = new Database(DBEngineType.MsSql);
                                    d.con_string =c_str;
                                    d.cmd_type = CommandType.StoredProcedure;
                                    d.text = "up850";
                                    d.add_parameter("ID", MainID);
                                    d.exec_non();
                                    if (File.Exists(sOutboundFolder + sEdiFile))
                                    {
                                        File.Move(sOutboundFolder + sEdiFile, sOutboundFolder + "\\Success\\" + sEdiFile);
                                    }
                                }

                            }
                            catch (Exception x)
                            {

                                Label2.BeginInvoke((new Action(() => { Label2.Text = ("Rollback format is " + format); })));
                                d = new Database(DBEngineType.MsSql);
                                d.con_string =c_str;
                                d.cmd_type = CommandType.StoredProcedure;
                                d.text = "delT850H";
                                d.add_parameter("MainID", MainID);
                                d.add_parameter("Msj", x.Message);
                                d.add_parameter("Fname", sEdiFile);

                                d.exec_non();
                                if (File.Exists(sOutboundFolder + sEdiFile))
                                {
                                    File.Move(sOutboundFolder + sEdiFile, sOutboundFolder + "\\Error\\" + sEdiFile);
                                }
                            }

                        }
                    }
                    else if (s856.Count() > 0 && s856 != null)
                    {
                        int MainID = 0;
                        format = "EDI 856";
                        Label2.BeginInvoke((new Action(() => { Label2.Text = ("EDI X12 format is " + format); })));
                        foreach (TS856 result in s856)
                        {
                            try
                            {



                                //  Indicates the start of a purchase order transaction set and assigns a control number.


                                var TransactionSetPurposeCode_01 = result.BSN.TransactionSetPurposeCode_01;
                                var ShipmentIdentification_02 = result.BSN.ShipmentIdentification_02;
                                var Date_03 = result.BSN.Date_03;
                                var Time_04 = result.BSN.Time_04;

                                d = new Database(DBEngineType.MsSql);
                                d.con_string =c_str;
                                d.cmd_type = CommandType.StoredProcedure;
                                d.text = "pT856H";
                                d.add_parameter("TransactionSetPurposeCode_01", TransactionSetPurposeCode_01 == null ? "" : TransactionSetPurposeCode_01);
                                d.add_parameter("ShipmentIdentification_02", ShipmentIdentification_02 == null ? "" : ShipmentIdentification_02);
                                d.add_parameter("Date_03", Date_03 == null ? "" : Date_03);
                                d.add_parameter("Time_04", Time_04 == null ? "" : Time_04);
                                d.add_parameter("Kind", "i");
                                d.add_parameter("@In2Out", 0);
                                d.add_parameter("@Out2In", 1);
                                MainID = Convert.ToInt32(d.exec_salar());

                                //DTM
                                if (result.DTM != null)
                                    foreach (var dtm in result.DTM)
                                    {
                                        d = new Database(DBEngineType.MsSql);
                                        d.con_string =c_str;
                                        d.cmd_type = CommandType.StoredProcedure;
                                        var DateTimeQualifier_01 = dtm.DateTimeQualifier_01;
                                        var Date_02 = dtm.Date_02;
                                        var Time_03 = dtm.Time_03;
                                        var TimeCode_04 = dtm.TimeCode_04;
                                        d.text = "pT856H_dtm";
                                        d.add_parameter("MainID", MainID);
                                        d.add_parameter("DateTimeQualifier_01", DateTimeQualifier_01 == null ? "" : DateTimeQualifier_01);
                                        d.add_parameter("Date_02", Date_02 == null ? "" : Date_02);
                                        d.add_parameter("Time_03", Time_03 == null ? "" : Time_03);
                                        d.add_parameter("TimeCode_04", TimeCode_04 == null ? "" : TimeCode_04);
                                        d.exec_non();


                                    }


                                //HL  MAIN
                                var hlMain = result.HLLoop[0];

                                d = new Database(DBEngineType.MsSql);
                                d.con_string =c_str;
                                d.cmd_type = CommandType.StoredProcedure;

                                var HierarchicalIDNumber_01 = hlMain.HL.HierarchicalIDNumber_01;
                                var HierarchicalLevelCode_03 = hlMain.HL.HierarchicalLevelCode_03;
                                var HierarchicalChildCode_04 = hlMain.HL.HierarchicalChildCode_04;
                                d.text = "pT856H_hl_main";
                                d.add_parameter("MainID", MainID);
                                d.add_parameter("HierarchicalIDNumber_01", HierarchicalIDNumber_01 == null ? "" : HierarchicalIDNumber_01);
                                d.add_parameter("HierarchicalLevelCode_03", HierarchicalLevelCode_03 == null ? "" : HierarchicalLevelCode_03);
                                d.add_parameter("HierarchicalChildCode_04", HierarchicalChildCode_04 == null ? "" : HierarchicalChildCode_04);
                                int HLID = Convert.ToInt32(d.exec_salar());

                                int i = 0;
                                int NID = 0;
                                //N1
                                if (hlMain.N1Loop != null)
                                    foreach (var n in hlMain.N1Loop)
                                    {
                                        if (i == 0)
                                        {
                                            d = new Database(DBEngineType.MsSql);
                                            d.con_string =c_str;
                                            d.cmd_type = CommandType.StoredProcedure;
                                            var EntityIdentifierCode_01 = n.N1.EntityIdentifierCode_01;
                                            var N1_Name_02 = n.N1.Name_02;
                                            var IdentificationCodeQualifier_03 = n.N1.IdentificationCodeQualifier_03;
                                            var IdentificationCode_04 = n.N1.IdentificationCode_04;
                                            d.text = "pT856H_n";
                                            d.add_parameter("MainID", MainID);
                                            d.add_parameter("Name_02", N1_Name_02 == null ? "" : N1_Name_02);
                                            d.add_parameter("EntityIdentifierCode_01", EntityIdentifierCode_01 == null ? "" : EntityIdentifierCode_01);
                                            d.add_parameter("IdentificationCodeQualifier_03", IdentificationCodeQualifier_03 == null ? "" : IdentificationCodeQualifier_03);
                                            d.add_parameter("IdentificationCode_04", IdentificationCode_04 == null ? "" : IdentificationCode_04);
                                            d.add_parameter("_index", i);
                                            NID = Convert.ToInt32(d.exec_salar());
                                        }
                                        else
                                        {
                                            //N1
                                            d = new Database(DBEngineType.MsSql);
                                            d.con_string =c_str;
                                            d.cmd_type = CommandType.StoredProcedure;
                                            var EntityIdentifierCode_01 = n.N1.EntityIdentifierCode_01;
                                            var N1_Name_02 = n.N1.Name_02;
                                            var IdentificationCodeQualifier_03 = n.N1.IdentificationCodeQualifier_03;
                                            var IdentificationCode_04 = n.N1.IdentificationCode_04;
                                            d.text = "pT856H_n_n1";
                                            d.add_parameter("MainID", MainID);
                                            d.add_parameter("NID", NID);
                                            d.add_parameter("Name_02", N1_Name_02 == null ? "" : N1_Name_02);
                                            d.add_parameter("EntityIdentifierCode_01", EntityIdentifierCode_01 == null ? "" : EntityIdentifierCode_01);

                                            d.add_parameter("IdentificationCodeQualifier_03", IdentificationCodeQualifier_03 == null ? "" : IdentificationCodeQualifier_03);
                                            d.add_parameter("IdentificationCode_04", IdentificationCode_04 == null ? "" : IdentificationCode_04);
                                            d.add_parameter("_index", i);
                                            d.exec_non();


                                            //  N2
                                            if (n.N2 != null)
                                                foreach (var n2 in n.N2)
                                                {
                                                    d = new Database(DBEngineType.MsSql);
                                                    d.con_string =c_str;
                                                    d.cmd_type = CommandType.StoredProcedure;
                                                    var Name_01 = n2.Name_01;
                                                    var Name_02 = n2.Name_02;
                                                    d.text = "pT856H_n_n2";
                                                    d.add_parameter("MainID", MainID);
                                                    d.add_parameter("NID", NID);
                                                    d.add_parameter("Name_01", Name_01 == null ? "" : Name_01);
                                                    d.add_parameter("_index", i);
                                                    d.add_parameter("Name_02", Name_02 == null ? "" : Name_02);
                                                    d.exec_non();

                                                } //  N3
                                            if (n.N3 != null)
                                                foreach (var n3 in n.N3)
                                                {
                                                    d = new Database(DBEngineType.MsSql);
                                                    d.con_string =c_str;
                                                    d.cmd_type = CommandType.StoredProcedure;
                                                    var AddressInformation_01 = n3.AddressInformation_01;
                                                    var AddressInformation_02 = n3.AddressInformation_02;
                                                    d.text = "pT856H_n_n3";
                                                    d.add_parameter("MainID", MainID);
                                                    d.add_parameter("NID", NID);
                                                    d.add_parameter("AddressInformation_01", AddressInformation_01 == null ? "" : AddressInformation_01);
                                                    d.add_parameter("_index", i);
                                                    d.add_parameter("AddressInformation_02", AddressInformation_02 == null ? "" : AddressInformation_02);
                                                    d.exec_non();

                                                }//  N4
                                            if (n.N4 != null)


                                            {
                                                d = new Database(DBEngineType.MsSql);
                                                d.con_string =c_str;
                                                d.cmd_type = CommandType.StoredProcedure;
                                                var n4 = n.N4;
                                                var CityName_01 = n4.CityName_01;
                                                var StateorProvinceCode_02 = n4.StateorProvinceCode_02;
                                                var PostalCode_03 = n4.PostalCode_03;
                                                var CountryCode_04 = n4.CountryCode_04;
                                                d.text = "pT856H_n_n4";
                                                d.add_parameter("MainID", MainID);
                                                d.add_parameter("NID", NID);
                                                d.add_parameter("_index", i);
                                                d.add_parameter("CityName_01", CityName_01 == null ? "" : CityName_01);
                                                d.add_parameter("StateorProvinceCode_02", StateorProvinceCode_02 == null ? "" : StateorProvinceCode_02);
                                                d.add_parameter("PostalCode_03", PostalCode_03 == null ? "" : PostalCode_03);
                                                d.add_parameter("CountryCode_04", CountryCode_04 == null ? "" : CountryCode_04);
                                                d.exec_non();

                                            }//  PER

                                        }

                                        i++;
                                    }
                                i = 0;
                                int k = 0;
                                //REF
                                if (hlMain.REF != null)
                                    foreach (var rf in hlMain.REF)
                                    {
                                        d = new Database(DBEngineType.MsSql);
                                        d.con_string =c_str;
                                        d.cmd_type = CommandType.StoredProcedure;
                                        var ReferenceIdentificationQualifier_01 = rf.ReferenceIdentificationQualifier_01;
                                        var ReferenceIdentification_02 = rf.ReferenceIdentification_02;
                                        d.text = "pT856H_ref";
                                        d.add_parameter("MainID", MainID);
                                        d.add_parameter("ReferenceIdentificationQualifier_01", ReferenceIdentificationQualifier_01 == null ? "" : ReferenceIdentificationQualifier_01);
                                        d.add_parameter("_index", k++);
                                        d.add_parameter("ReferenceIdentification_02", ReferenceIdentification_02 == null ? "" : ReferenceIdentification_02);
                                        d.exec_non();
                                    }


                                //sn1
                                if (hlMain.SN1 != null)
                                {
                                    d = new Database(DBEngineType.MsSql);
                                    d.con_string =c_str;
                                    d.cmd_type = CommandType.StoredProcedure;
                                    var AssignedIdentification_01 = hlMain.SN1.AssignedIdentification_01;
                                    var NumberofUnitsShipped_02 = hlMain.SN1.NumberofUnitsShipped_02;
                                    var UnitorBasisforMeasurementCode_03 = hlMain.SN1.UnitorBasisforMeasurementCode_03;
                                    d.text = "pT856H_sn1";
                                    d.add_parameter("MainID", MainID);
                                    d.add_parameter("AssignedIdentification_01", AssignedIdentification_01 == null ? "" : AssignedIdentification_01);
                                    d.add_parameter("NumberofUnitsShipped_02", NumberofUnitsShipped_02 == null ? "" : NumberofUnitsShipped_02);
                                    d.add_parameter("UnitorBasisforMeasurementCode_03", UnitorBasisforMeasurementCode_03 == null ? "" : UnitorBasisforMeasurementCode_03);
                                    d.exec_non();
                                }
                                //TD1
                                if (hlMain.TD1 != null)
                                    foreach (var td1 in hlMain.TD1)
                                    {
                                        d = new Database(DBEngineType.MsSql);
                                        d.con_string =c_str;
                                        d.cmd_type = CommandType.StoredProcedure;
                                        var WeightQualifier_06 = td1.WeightQualifier_06;
                                        var Weight_07 = td1.Weight_07;
                                        var UnitorBasisforMeasurementCode_08 = td1.UnitorBasisforMeasurementCode_08;
                                        var Volume_09 = td1.Volume_09;
                                        var UnitorBasisforMeasurementCode_10 = td1.UnitorBasisforMeasurementCode_10;
                                        d.text = "pT856H_td1";
                                        d.add_parameter("MainID", MainID);
                                        d.add_parameter("WeightQualifier_06", WeightQualifier_06 == null ? "" : WeightQualifier_06);
                                        d.add_parameter("Weight_07", Weight_07 == null ? "" : Weight_07);
                                        d.add_parameter("UnitorBasisforMeasurementCode_08", UnitorBasisforMeasurementCode_08 == null ? "" : UnitorBasisforMeasurementCode_08);
                                        d.add_parameter("Volume_09", Volume_09 == null ? "" : Volume_09);
                                        d.add_parameter("UnitorBasisforMeasurementCode_10", UnitorBasisforMeasurementCode_10 == null ? "" : UnitorBasisforMeasurementCode_10);
                                        d.exec_non();

                                    }





                                // HL line
                                int j = 0;
                                for (int kl =1; kl < result.HLLoop.Count(); kl++)
                                {
                                    var  hl = result.HLLoop[kl];
                                    // HL
                                    d = new Database(DBEngineType.MsSql);
                                    d.con_string =c_str;
                                    d.cmd_type = CommandType.StoredProcedure;
                                    var hHierarchicalIDNumber_01 = hl.HL.HierarchicalIDNumber_01;
                                    var hHierarchicalParentIDNumber_02 = hl.HL.HierarchicalParentIDNumber_02;
                                    var hHierarchicalLevelCode_03 = hl.HL.HierarchicalLevelCode_03;
                                    var hHierarchicalChildCode_04 = hl.HL.HierarchicalChildCode_04;
                                    d.text = "pT856H_hl";
                                    d.add_parameter("MainID", MainID);
                                    d.add_parameter("HierarchicalIDNumber_01", HierarchicalIDNumber_01 == null ? "" : HierarchicalIDNumber_01);
                                    d.add_parameter("HierarchicalParentIDNumber_02", hHierarchicalParentIDNumber_02 == null ? "" : hHierarchicalParentIDNumber_02);
                                    d.add_parameter("HierarchicalLevelCode_03", hHierarchicalLevelCode_03 == null ? "" : hHierarchicalLevelCode_03);
                                    d.add_parameter("HierarchicalChildCode_04", hHierarchicalChildCode_04 == null ? "" : hHierarchicalChildCode_04);
                                    int lHLID = Convert.ToInt32(d.exec_salar());

                                    //LN
                                    if (hl.LIN != null)
                                    {
                                        d = new Database(DBEngineType.MsSql);
                                        d.con_string =c_str;
                                        d.cmd_type = CommandType.StoredProcedure;
                                        var hAssignedIdentification_01 = hl.LIN.AssignedIdentification_01;
                                        var hProductServiceIDQualifier_02 = hl.LIN.ProductServiceIDQualifier_02;
                                        var hProductServiceID_03 = hl.LIN.ProductServiceID_03;
                                        var hProductServiceIDQualifier_04 = hl.LIN.ProductServiceIDQualifier_04;
                                        var hProductServiceID_05 = hl.LIN.ProductServiceID_05;
                                        var hProductServiceIDQualifier_06 = hl.LIN.ProductServiceIDQualifier_06;
                                        var hProductServiceID_07 = hl.LIN.ProductServiceID_07;
                                        var hProductServiceIDQualifier_08 = hl.LIN.ProductServiceIDQualifier_08;
                                        var hProductServiceID_09 = hl.LIN.ProductServiceID_09;
                                        var hProductServiceIDQualifier_10 = hl.LIN.ProductServiceIDQualifier_10;
                                        var hProductServiceID_11 = hl.LIN.ProductServiceID_11;
                                        d.text = "pT856H_hl_lin";
                                        d.add_parameter("MainID", MainID);
                                        d.add_parameter("HLID", lHLID);
                                        d.add_parameter("hAssignedIdentification_01", hAssignedIdentification_01 == null ? "" : hAssignedIdentification_01);
                                        d.add_parameter("hProductServiceIDQualifier_02", hProductServiceIDQualifier_02 == null ? "" : hProductServiceIDQualifier_02);
                                        d.add_parameter("hProductServiceID_03", hProductServiceID_03 == null ? "" : hProductServiceID_03);
                                        d.add_parameter("hProductServiceIDQualifier_04", hProductServiceIDQualifier_04 == null ? "" : hProductServiceIDQualifier_04);
                                        d.add_parameter("hProductServiceID_05", hProductServiceID_05 == null ? "" : hProductServiceID_05);
                                        d.add_parameter("hProductServiceIDQualifier_06", hProductServiceIDQualifier_06 == null ? "" : hProductServiceIDQualifier_06);
                                        d.add_parameter("hProductServiceID_07", hProductServiceID_07 == null ? "" : hProductServiceID_07);
                                        d.add_parameter("hProductServiceIDQualifier_08", hProductServiceIDQualifier_08 == null ? "" : hProductServiceIDQualifier_08);
                                        d.add_parameter("hProductServiceID_09", hProductServiceID_09 == null ? "" : hProductServiceID_09);
                                        d.add_parameter("hProductServiceIDQualifier_10", hProductServiceIDQualifier_10 == null ? "" : hProductServiceIDQualifier_10);
                                        d.add_parameter("hProductServiceID_11", hProductServiceID_11 == null ? "" : hProductServiceID_11);
                                        d.exec_non();



                                    }
                                    // SN
                                    if (hl.SN1 != null)
                                    {
                                        d = new Database(DBEngineType.MsSql);
                                        d.con_string =c_str;
                                        d.cmd_type = CommandType.StoredProcedure;
                                        var snAssignedIdentification_01 = hl.SN1.AssignedIdentification_01;
                                        var snNumberofUnitsShipped_02 = hl.SN1.NumberofUnitsShipped_02;
                                        var snUnitorBasisforMeasurementCode_03 = hl.SN1.UnitorBasisforMeasurementCode_03;
                                        d.text = "pT856H_hl_sn1";
                                        d.add_parameter("MainID", MainID);
                                        d.add_parameter("HLID", lHLID);
                                        d.add_parameter("snAssignedIdentification_01", snAssignedIdentification_01 == null ? "" : snAssignedIdentification_01);
                                        d.add_parameter("snNumberofUnitsShipped_02", snNumberofUnitsShipped_02 == null ? "" : snNumberofUnitsShipped_02);
                                        d.add_parameter("snUnitorBasisforMeasurementCode_03", snUnitorBasisforMeasurementCode_03 == null ? "" : snUnitorBasisforMeasurementCode_03);
                                        d.exec_non();

                                    }
                                    //PRF
                                    if (hl.PRF != null)
                                    {
                                        d = new Database(DBEngineType.MsSql);
                                        d.con_string =c_str;
                                        d.cmd_type = CommandType.StoredProcedure;
                                        var PurchaseOrderNumber_01 = hl.PRF.PurchaseOrderNumber_01;
                                        var ReleaseNumber_02 = hl.PRF.ReleaseNumber_02;
                                        d.text = "pT856H_hl_prf";
                                        d.add_parameter("MainID", MainID);
                                        d.add_parameter("HLID", HLID);
                                        d.add_parameter("PurchaseOrderNumber_01", PurchaseOrderNumber_01 == null ? "" : PurchaseOrderNumber_01);
                                        d.add_parameter("ReleaseNumber_02", ReleaseNumber_02 == null ? "" : ReleaseNumber_02);
                                        d.exec_non();
                                    }
                                    //PID
                                    if (hl.PID != null)
                                        foreach (var pid in hl.PID)
                                        {
                                            d = new Database(DBEngineType.MsSql);
                                            d.con_string =c_str;
                                            d.cmd_type = CommandType.StoredProcedure;

                                            var ItemDescriptionType_01 = pid.ItemDescriptionType_01;
                                            var Description_05 = pid.Description_05;
                                            d.text = "pT856H_hl_pid";
                                            d.add_parameter("MainID", MainID);
                                            d.add_parameter("HLID", lHLID);
                                            d.add_parameter("ItemDescriptionType_01", ItemDescriptionType_01 == null ? "" : ItemDescriptionType_01);
                                            d.add_parameter("Description_05", Description_05 == null ? "" : Description_05);
                                            d.exec_non();
                                        }

                                    //TD
                                    if (hl.TD1 != null)
                                        foreach (var td1 in hl.TD1)
                                        {
                                            d = new Database(DBEngineType.MsSql);
                                            d.con_string =c_str;
                                            d.cmd_type = CommandType.StoredProcedure;
                                            var WeightQualifier_06 = td1.WeightQualifier_06;
                                            var Weight_07 = td1.Weight_07;
                                            var UnitorBasisforMeasurementCode_08 = td1.UnitorBasisforMeasurementCode_08;
                                            var Volume_09 = td1.Volume_09;
                                            var UnitorBasisforMeasurementCode_10 = td1.UnitorBasisforMeasurementCode_10;
                                            d.text = "pT856H_hl_td1";
                                            d.add_parameter("MainID", MainID);
                                            d.add_parameter("HLID", lHLID);
                                            d.add_parameter("WeightQualifier_06", WeightQualifier_06 == null ? "" : WeightQualifier_06);
                                            d.add_parameter("Weight_07", Weight_07 == null ? "" : Weight_07);
                                            d.add_parameter("UnitorBasisforMeasurementCode_08", UnitorBasisforMeasurementCode_08 == null ? "" : UnitorBasisforMeasurementCode_08);
                                            d.add_parameter("Volume_09", Volume_09 == null ? "" : Volume_09);
                                            d.add_parameter("UnitorBasisforMeasurementCode_10", UnitorBasisforMeasurementCode_10 == null ? "" : UnitorBasisforMeasurementCode_10);
                                            d.exec_non();

                                        }

                                    if (hl.REF != null)
                                        foreach (var rf in hl.REF)
                                        {
                                            d = new Database(DBEngineType.MsSql);
                                            d.con_string =c_str;
                                            d.cmd_type = CommandType.StoredProcedure;
                                            var ReferenceIdentificationQualifier_01 = rf.ReferenceIdentificationQualifier_01;
                                            var ReferenceIdentification_02 = rf.ReferenceIdentification_02;
                                            d.text = "pT856H_hl_ref";
                                            d.add_parameter("MainID", MainID);
                                            d.add_parameter("HLID", lHLID);
                                            d.add_parameter("ReferenceIdentificationQualifier_01", ReferenceIdentificationQualifier_01 == null ? "" : ReferenceIdentificationQualifier_01);
                                            d.add_parameter("ReferenceIdentification_02", ReferenceIdentification_02 == null ? "" : ReferenceIdentification_02);
                                            d.exec_non();
                                        }





                                }
                            

                                if (result.HasErrors)
                                {
                                    Label2.BeginInvoke((new Action(() => { Label2.Text = ("Rollback format is " + format); })));
                                    d = new Database(DBEngineType.MsSql);
                                    d.con_string =c_str;
                                    d.cmd_type = CommandType.StoredProcedure;
                                    d.text = "delT856H";
                                    d.add_parameter("MainID", MainID);
                                    d.add_parameter("Fname", sEdiFile);
                                    d.exec_non();
                                    if (File.Exists(sOutboundFolder + sEdiFile))
                                    {
                                        File.Move(sOutboundFolder + sEdiFile, sOutboundFolder + "\\Error\\" + sEdiFile);
                                    }
                                }
                                else
                                {

                                    d = new Database(DBEngineType.MsSql);
                                    d.con_string =c_str;
                                    d.cmd_type = CommandType.StoredProcedure;
                                    d.text = "up856";
                                    d.add_parameter("ID", MainID);
                                    d.exec_non();
                                    if (File.Exists(sOutboundFolder + sEdiFile))
                                    {
                                        File.Move(sOutboundFolder + sEdiFile, sOutboundFolder + "\\Success\\" + sEdiFile);
                                    }
                                }

                            }
                            catch (Exception x)
                            {
                                Label2.BeginInvoke((new Action(() => { Label2.Text = ("Rollback format is " + format); })));
                                d = new Database(DBEngineType.MsSql);
                                d.con_string =c_str;
                                d.cmd_type = CommandType.StoredProcedure;
                                d.text = "delT856H";
                                d.add_parameter("Msj", x.Message.ToString());

                                d.add_parameter("MainID", MainID);
                                d.add_parameter("Fname", sEdiFile);

                                d.exec_non();

                                if (File.Exists(sOutboundFolder + sEdiFile))
                                {
                                    File.Move(sOutboundFolder + sEdiFile, sOutboundFolder + "\\Error\\" + sEdiFile);
                                }

                            }


                        }


                    } 
                

                    else if (s888 != null && s888.Count() > 0)
                    {
                        format = "EDI 888";
                        Label2.BeginInvoke((new Action(() => { Label2.Text = ("EDI X12 format is " + format); })));
                        int MainID = 0;

                        foreach (TS888 result in s888)
                        {
                            try
                            {

                                d = new Database(DBEngineType.MsSql);
                                d.con_string =c_str;
                                d.cmd_type = CommandType.StoredProcedure;
                                //  Indicates the start of a purchase order transaction set and assigns a control number.


                                var TransactionSetPurposeCode_01 = result.BGN.TransactionSetPurposeCode_01;// = "00";
                                var ReferenceIdentification_02 = result.BGN.ReferenceIdentification_02;// = "SA";
                                var Date_03 = result.BGN.Date_03;//= "XX-1234";
                                var Time_04 = result.BGN.Time_04;// = "20170301";                                      
                                d.text = "pT888H";
                                d.add_parameter("TransactionSetPurposeCode_01", TransactionSetPurposeCode_01==null?"": TransactionSetPurposeCode_01);
                                d.add_parameter("ReferenceIdentification_02", ReferenceIdentification_02==null?"": ReferenceIdentification_02);
                                d.add_parameter("Date_03", Date_03==null?"": Date_03);
                                d.add_parameter("Time_04", Time_04==null?"": Time_04);
                                d.add_parameter("@In2Out", 0);
                                d.add_parameter("@Out2In", 1);
                           MainID = Convert.ToInt32(d.exec_salar());

                                if (result.N1Loop != null)
                                    foreach (var n in result.N1Loop)
                                    {
                                        d = new Database(DBEngineType.MsSql);
                                        d.con_string =c_str;
                                        d.cmd_type = CommandType.StoredProcedure;

                                        var EntityIdentifierCode_01 = n.N1.EntityIdentifierCode_01;
                                        var  N1_Name_02 = n.N1.Name_02;
                                        var IdentificationCodeQualifier_03 = n.N1.IdentificationCodeQualifier_03;
                                        var IdentificationCode_04 = n.N1.IdentificationCode_04;
                                        d.text = "pT888H_n1";
                                        d.add_parameter("MainID", MainID);
                                        d.add_parameter("EntityIdentifierCode_01", EntityIdentifierCode_01==null?"": EntityIdentifierCode_01);
                                        d.add_parameter("Name_02", N1_Name_02==null?"": N1_Name_02);
                                        d.add_parameter("IdentificationCodeQualifier_03", IdentificationCodeQualifier_03==null?"": IdentificationCodeQualifier_03);
                                        d.add_parameter("IdentificationCode_04", IdentificationCode_04 == null?"": IdentificationCode_04);
                                        d.exec_non();
                                    }
                                //N9
                                if (result.N9 != null)
                                    foreach (var n9 in result.N9)
                                    {
                                        d = new Database(DBEngineType.MsSql);
                                        d.con_string =c_str;
                                        d.cmd_type = CommandType.StoredProcedure;
                                        var n9ReferenceIdentificationQualifier_01 = n9.ReferenceIdentificationQualifier_01;
                                        var n9ReferenceIdentification_02 = n9.ReferenceIdentification_02;
                                        d.text = "pT888H_n9";
                                        d.add_parameter("MainID", MainID);
                                        d.add_parameter("ReferenceIdentificationQualifier_01", n9ReferenceIdentificationQualifier_01==null?"": n9ReferenceIdentificationQualifier_01);
                                        d.add_parameter("ReferenceIdentification_02", n9ReferenceIdentification_02==null?"": n9ReferenceIdentification_02); 
                                        d.exec_non();
                                    }
                                d = new Database(DBEngineType.MsSql);
                                d.con_string =c_str;
                                d.cmd_type = CommandType.StoredProcedure;
                                //G62
                                var g62DateQualifier_01 = result.G62.DateQualifier_01;
                                var g62Date_02 = result.G62.Date_02;
                                d.text = "pT888H_g62";
                                d.add_parameter("MainID", MainID);
                                d.add_parameter("g62DateQualifier_01", g62DateQualifier_01==null?"": g62DateQualifier_01);
                                d.add_parameter("g62Date_02", g62Date_02==null?"": g62Date_02);
                                d.exec_non();

                                //G53
                                foreach (var g53 in result.G53Loop)
                                {

                                    d = new Database(DBEngineType.MsSql);
                                    d.con_string =c_str;
                                    d.cmd_type = CommandType.StoredProcedure;
                                    var MaintenanceTypeCode_01 = g53.G53.MaintenanceTypeCode_01;

                                    d.text = "pT888H_g53";
                                    d.add_parameter("MainID", MainID);
                                    d.add_parameter("MaintenanceTypeCode_01", MaintenanceTypeCode_01==null?"": MaintenanceTypeCode_01);
                                   int G53ID =Convert.ToInt32(d.exec_salar());
                                    d = new Database(DBEngineType.MsSql);
                                    d.con_string =c_str;
                                    d.cmd_type = CommandType.StoredProcedure;
                                    var AssignedNumber_0 = g53.NTE[0].NoteReferenceCode_01.ToString();

                                    d.text = "pT888H_g53_lx";
                                    d.add_parameter("MainID", MainID);
                                    d.add_parameter("G53ID", G53ID);
                                    d.add_parameter("AssignedNumber_01", AssignedNumber_0 == null ? "" : AssignedNumber_0);

                                    d.exec_non();


                                    foreach (var g39 in g53.G39Loop)
                                    {
                                        d = new Database(DBEngineType.MsSql);
                                        d.con_string =c_str;
                                        d.cmd_type = CommandType.StoredProcedure;
                                        var ProductServiceIDQualifier_02 = g39.G39.ProductServiceIDQualifier_02;
                                        var ProductServiceID_03 = g39.G39.ProductServiceID_03;
                                        var SpecialHandlingCode_04 = g39.G39.SpecialHandlingCode_04;
                                        var UnitWeight_05 = g39.G39.UnitWeight_05;
                                        var WeightQualifier_06 = g39.G39.WeightQualifier_06;
                                        var WeightUnitCode_07 = g39.G39.WeightUnitCode_07;
                                        var Height_08 = g39.G39.Height_08;
                                        var UnitorBasisforMeasurementCode_09 = g39.G39.UnitorBasisforMeasurementCode_09;
                                        var Width_10 = g39.G39.Width_10;
                                        var UnitorBasisforMeasurementCode_11 = g39.G39.UnitorBasisforMeasurementCode_11;
                                        var Length_12 = g39.G39.Length_12;
                                        var UnitorBasisforMeasurementCode_13 = g39.G39.UnitorBasisforMeasurementCode_13;
                                        var Volume_14 = g39.G39.Volume_14;
                                        var UnitorBasisforMeasurementCode_15 = g39.G39.UnitorBasisforMeasurementCode_15;
                                        var UnitorBasisforMeasurementCode_19 = g39.G39.UnitorBasisforMeasurementCode_19;
                                        var ProductServiceIDQualifier_23 = g39.G39.ProductServiceIDQualifier_23;
                                        var ProductServiceID_24 = g39.G39.ProductServiceID_24;
                                        d.text = "pT888H_g53_g39";
                                        d.add_parameter("MainID", MainID);
                                        d.add_parameter("G53ID", G53ID);
                                        d.add_parameter("ProductServiceIDQualifier_02", ProductServiceIDQualifier_02==null?"": ProductServiceIDQualifier_02);
                                        d.add_parameter("ProductServiceID_03", ProductServiceID_03==null?"": ProductServiceID_03);
                                        d.add_parameter("SpecialHandlingCode_04", SpecialHandlingCode_04==null?"": SpecialHandlingCode_04);
                                        d.add_parameter("UnitWeight_05", UnitWeight_05==null?"": UnitWeight_05);
                                        d.add_parameter("WeightQualifier_06", WeightQualifier_06==null?"": WeightQualifier_06);
                                        d.add_parameter("WeightUnitCode_07", WeightUnitCode_07==null?"": WeightUnitCode_07);
                                        d.add_parameter("Height_08", Height_08==null?"": Height_08);
                                        d.add_parameter("UnitorBasisforMeasurementCode_09", UnitorBasisforMeasurementCode_09==null?"": UnitorBasisforMeasurementCode_09);
                                        d.add_parameter("Width_10", Width_10==null?"": Width_10);
                                        d.add_parameter("UnitorBasisforMeasurementCode_11", UnitorBasisforMeasurementCode_11==null?"": UnitorBasisforMeasurementCode_11);
                                        d.add_parameter("Length_12", Length_12==null?"": Length_12);
                                        d.add_parameter("UnitorBasisforMeasurementCode_13", UnitorBasisforMeasurementCode_13==null?"": UnitorBasisforMeasurementCode_13);
                                        d.add_parameter("Volume_14", Volume_14==null?"": Volume_14);
                                        d.add_parameter("UnitorBasisforMeasurementCode_15", UnitorBasisforMeasurementCode_15==null?"": UnitorBasisforMeasurementCode_15);
                                        d.add_parameter("UnitorBasisforMeasurementCode_19", UnitorBasisforMeasurementCode_19==null?"": UnitorBasisforMeasurementCode_19);
                                        d.add_parameter("ProductServiceIDQualifier_23", ProductServiceIDQualifier_23==null?"": ProductServiceIDQualifier_23);
                                        d.add_parameter("ProductServiceID_24", ProductServiceID_24==null?"": ProductServiceID_24);
                                      int G39ID= Convert.ToInt32(d.exec_salar());
                                        if(g39.N9!=null)
                                        foreach (var n9 in g39.N9)
                                        {
                                            d = new Database(DBEngineType.MsSql);
                                            d.con_string =c_str;
                                            d.cmd_type = CommandType.StoredProcedure;
                                            var n9ReferenceIdentificationQualifier_01 = n9.ReferenceIdentificationQualifier_01;
                                            var n9ReferenceIdentification_02 = n9.ReferenceIdentification_02;
                                            d.text = "pT888H_g53_g39_n9";
                                            d.add_parameter("MainID", MainID);
                                            d.add_parameter("G39ID", G39ID);
                                            d.add_parameter("ReferenceIdentificationQualifier_01", n9ReferenceIdentificationQualifier_01==null?"": n9ReferenceIdentificationQualifier_01);
                                            d.add_parameter("ReferenceIdentification_02", n9ReferenceIdentification_02==null?"": n9ReferenceIdentification_02);
                                            d.exec_non();
                                        }
                                        if (g39.H1 != null)
                                            foreach (var h1 in g39.H1)
                                        {
                                            d = new Database(DBEngineType.MsSql);
                                            d.con_string =c_str;
                                            d.cmd_type = CommandType.StoredProcedure;
                                            var ItemDescriptionTypeCode_01 = h1.HazardousMaterialCode_01.ToString();
                                            var Description_05 = h1.HazardousMaterialContact_05.ToString();
                                            var LanguageCode_09 = h1.PackingGroupCode_09.ToString();
                                            d.text = "pT888H_g53_g39_pid";
                                            d.add_parameter("MainID", MainID);
                                            d.add_parameter("G39ID", G39ID);
                                            d.add_parameter("ItemDescriptionTypeCode_01", ItemDescriptionTypeCode_01 == null?"": ItemDescriptionTypeCode_01);
                                            d.add_parameter("Description_05", Description_05 == null?"": Description_05);
                                            d.add_parameter("LanguageCode_09", LanguageCode_09 == null?"": LanguageCode_09);
                                            d.exec_non();
                                        }
                                    }



                                }
                              

                                if (result.HasErrors)
                                {
                                    Label2.BeginInvoke((new Action(() => { Label2.Text = ("Rollback format is " + format); })));
                                    d = new Database(DBEngineType.MsSql);
                                    d.con_string =c_str;
                                    d.cmd_type = CommandType.StoredProcedure;
                                    d.text = "delT888H";
                                    d.add_parameter("MainID", MainID);
                                    d.add_parameter("Fname", sEdiFile);
                                    d.exec_non();
                                    if (File.Exists(sOutboundFolder + sEdiFile))
                                    {
                                        File.Move(sOutboundFolder + sEdiFile, sOutboundFolder + "\\Error\\" + sEdiFile);
                                    }
                                }

                                else
                                {
                                    d = new Database(DBEngineType.MsSql);
                                    d.con_string =c_str;
                                    d.cmd_type = CommandType.StoredProcedure;
                                    d.text = "up888";
                                    d.add_parameter("ID", MainID);
                                    d.exec_non();

                                    str = str.Replace("~NTE*", "~LX*");
                                    str = str.Replace("~H1*", "~PID*");
                                    File.WriteAllText(sEdiPathFile, str);

                                    if (File.Exists(sOutboundFolder + sEdiFile))
                                    {
                                        File.Move(sOutboundFolder + sEdiFile, sOutboundFolder + "Success\\" + sEdiFile);
                                    }
 
                                }

                                }
                            catch (Exception x)
                            {
                                Label2.BeginInvoke((new Action(() => { Label2.Text = ("Rollback format is " + format); })));
                                d = new Database(DBEngineType.MsSql);
                                d.con_string =c_str;
                                d.cmd_type = CommandType.StoredProcedure;
                                d.text = "delT888H";
                                d.add_parameter("MainID", MainID);
                            d.add_parameter("Msj", x.Message);
                                d.add_parameter("Fname", sEdiFile);
                                d.exec_non();
                                if (File.Exists(sOutboundFolder + sEdiFile))
                                {
                                    File.Move(sOutboundFolder + sEdiFile, sOutboundFolder + "\\Error\\" + sEdiFile);
                                }


                            }
                        }
                    }

                    else if (s944 != null && s944.Count()>0)
                    {
                        int MainID = 0;
                        format = "EDI 944";
                        Label2.BeginInvoke((new Action(() => { Label2.Text = ("EDI X12 format is " + format); })));
                        foreach (TS944 result in s944)
                        {

                            //  Indicates the start of a purchase order transaction set and assigns a control number.
                            try
                            {

                                  d = new Database(DBEngineType.MsSql);
                                d.con_string =c_str;
                                d.cmd_type = CommandType.StoredProcedure;
                                //  Indicates the start of a purchase order transaction set and assigns a control number.


                                var ReportingCode_01 = result.W17.ReportingCode_01;
                                var Date_02 = result.W17.Date_02;
                                var WarehouseReceiptNumber_03 = result.W17.WarehouseReceiptNumber_03;
                                var ShipmentIdentificationNumber_05 = result.W17.ShipmentIdentificationNumber_05;
                                var TimeQualifier_06 = result.W17.TimeQualifier_06;
                                var Time_07 = result.W17.Time_07;
                                var w14QuantityReceived_01 = result.W14.QuantityReceived_01;


                                d.text = "pT944H";
                                d.add_parameter("ReportingCode_01", ReportingCode_01==null?"": ReportingCode_01);
                                d.add_parameter("Date_02", Date_02==null?"": Date_02);
                                d.add_parameter("WarehouseReceiptNumber_03", WarehouseReceiptNumber_03==null?"": WarehouseReceiptNumber_03); 
                                d.add_parameter("ShipmentIdentificationNumber_05", ShipmentIdentificationNumber_05==null?"": ShipmentIdentificationNumber_05);
                                d.add_parameter("TimeQualifier_06", TimeQualifier_06==null?"": TimeQualifier_06);
                                d.add_parameter("w14QuantityReceived_01", w14QuantityReceived_01==null?"": w14QuantityReceived_01);
                                d.add_parameter("Time_07", Time_07==null?"": Time_07);
                                d.add_parameter("@In2Out", 0);
                                d.add_parameter("@Out2In", 1);
                           MainID = Convert.ToInt32(d.exec_salar());


                                //  N1
                                if (result.N1Loop != null)
                                    foreach (var n in result.N1Loop)
                                    {
                                        d = new Database(DBEngineType.MsSql);
                                        d.con_string =c_str;
                                        d.cmd_type = CommandType.StoredProcedure;

                                        var EntityIdentifierCode_01 = n.N1.EntityIdentifierCode_01;
                                        var N1_Name_02 = n.N1.Name_02;
                                        var IdentificationCodeQualifier_03 = n.N1.IdentificationCodeQualifier_03;
                                        var IdentificationCode_04 = n.N1.IdentificationCode_04;
                                        d.text = "pT944H_n1";
                                        d.add_parameter("MainID", MainID);
                                        d.add_parameter("EntityIdentifierCode_01", EntityIdentifierCode_01==null?"": EntityIdentifierCode_01);
                                        d.add_parameter("Name_02", N1_Name_02==null?  "": N1_Name_02);
                                        d.add_parameter("IdentificationCodeQualifier_03", IdentificationCodeQualifier_03==null?"":IdentificationCodeQualifier_03);
                                        d.add_parameter("IdentificationCode_04", IdentificationCode_04==null?"": IdentificationCode_04);
                                      
                                        d.exec_non();


                                    }
                                //  N9
                                foreach (var n in result.N9)
                                {
                                    d = new Database(DBEngineType.MsSql);
                                    d.con_string =c_str;
                                    d.cmd_type = CommandType.StoredProcedure;

                                    var ReferenceIdentificationQualifier_01 = n.ReferenceIdentificationQualifier_01;
                                    var ReferenceIdentification_02 = n.ReferenceIdentification_02;
                                    d.text = "pT944H_n9";
                                    d.add_parameter("MainID", MainID);
                                    d.add_parameter("ReferenceIdentificationQualifier_01", ReferenceIdentificationQualifier_01==null?"": ReferenceIdentificationQualifier_01);
                                    d.add_parameter("ReferenceIdentification_02", ReferenceIdentification_02==null?"": ReferenceIdentification_02); 
                                    d.exec_non();

                                }
                                //  W07
                                foreach (var w in result.W07Loop)
                                {
                                    d = new Database(DBEngineType.MsSql);
                                    d.con_string =c_str;
                                    d.cmd_type = CommandType.StoredProcedure;
                                    var wQuantityReceived_01 = w.W07.QuantityReceived_01;
                                    var UnitorBasisforMeasurementCode_02 = w.W07.UnitorBasisforMeasurementCode_02;
                                    var ProductServiceIDQualifier_04 = w.W07.ProductServiceIDQualifier_04;
                                    var ProductServiceID_05 = w.W07.ProductServiceID_05;
                                    var ProductServiceIDQualifier_06 = w.W07.ProductServiceIDQualifier_06;
                                    var ProductServiceID_07 = w.W07.ProductServiceID_07;
                                    var ProductServiceIDQualifier_10 = w.W07.ProductServiceIDQualifier_10;
                                    var ProductServiceID_11 = w.W07.ProductServiceID_11;

                                    d.text = "pT944H_w07";
                                    d.add_parameter("MainID", MainID);
                                    d.add_parameter("wQuantityReceived_01", wQuantityReceived_01==null?"": wQuantityReceived_01);
                                    d.add_parameter("UnitorBasisforMeasurementCode_02", UnitorBasisforMeasurementCode_02==null?"": UnitorBasisforMeasurementCode_02);
                                    d.add_parameter("ProductServiceIDQualifier_04", ProductServiceIDQualifier_04==null?"": ProductServiceIDQualifier_04);
                                    d.add_parameter("ProductServiceID_05", ProductServiceID_05==null?"": ProductServiceID_05);
                                    d.add_parameter("ProductServiceIDQualifier_06", ProductServiceIDQualifier_06==null?"": ProductServiceIDQualifier_06);
                                    d.add_parameter("ProductServiceID_07", ProductServiceID_07==null?"": ProductServiceID_07);
                                    d.add_parameter("ProductServiceIDQualifier_10", ProductServiceIDQualifier_10==null?"": ProductServiceIDQualifier_10);
                                    d.add_parameter("ProductServiceID_11", ProductServiceID_11==null?"": ProductServiceID_11);
                                    d.exec_non();
                                }


                                if (result.HasErrors)
                                {
                                    Label2.BeginInvoke((new Action(() => { Label2.Text = ("Rollback format is " + format); })));
                                    d = new Database(DBEngineType.MsSql);
                                    d.con_string =c_str;
                                    d.cmd_type = CommandType.StoredProcedure;
                                    d.text = "delT944H";
                                    d.add_parameter("MainID", MainID);
                                    d.add_parameter("Fname", sEdiFile);
                                    d.exec_non();
                                    if (File.Exists(sOutboundFolder + sEdiFile))
                                    {
                                        File.Move(sOutboundFolder + sEdiFile, sOutboundFolder + "\\Error\\" + sEdiFile);
                                    }
                                } else
                                {

                                    d = new Database(DBEngineType.MsSql);
                                    d.con_string =c_str;
                                    d.cmd_type = CommandType.StoredProcedure;
                                    d.text = "up944";
                                    d.add_parameter("ID", MainID);
                                    d.exec_non();

                                    if (File.Exists(sOutboundFolder + sEdiFile))
                                    {
                                        File.Move(sOutboundFolder + sEdiFile, sOutboundFolder + "\\Success\\" + sEdiFile);
                                    }

                                }
                            }
                            catch(Exception x)
                            {

                                Label2.BeginInvoke((new Action(() => { Label2.Text = ("Rollback format is " + format); })));
                                  d = new Database(DBEngineType.MsSql);
                                d.con_string =c_str;
                                d.cmd_type = CommandType.StoredProcedure;
                                d.text = "delT944H";
                              d.add_parameter("Msj", x.Message.ToString());
                                d.add_parameter("MainID", MainID);
                                d.add_parameter("Fname", sEdiFile);
                                d.exec_non();
                                if (File.Exists(sOutboundFolder + sEdiFile))
                                {
                                    File.Move(sOutboundFolder + sEdiFile, sOutboundFolder + "\\Error\\" + sEdiFile);
                                }
                            }
                            
                            }

                    }
                    else if (s947 != null && s947.Count()> 0)
                    {
                        format = "EDI 947";
                        Label2.BeginInvoke((new Action(() => { Label2.Text = ("EDI X12 format is " + format); })));
                        int MainID = 0;
                        foreach (TS947 result in s947)
                        {
                            try
                            {

                                  d = new Database(DBEngineType.MsSql);
                                d.con_string =c_str;
                                d.cmd_type = CommandType.StoredProcedure;
                                //  Indicates the start of a purchase order transaction set and assigns a control number.


                                var Date_01 = result.W15.Date_01;
                                var AdjustmentNumber_02 = result.W15.AdjustmentNumber_02;
                                var AdjustmentNumber_03 = result.W15.AdjustmentNumber_03;
                                d.text = "pT947H";
                                d.add_parameter("Date_01", Date_01==null?"": Date_01);
                                d.add_parameter("AdjustmentNumber_02", AdjustmentNumber_02==null?"": AdjustmentNumber_02);
                                d.add_parameter("AdjustmentNumber_03", AdjustmentNumber_03==null?"": AdjustmentNumber_03); 
                                d.add_parameter("@In2Out", 0);
                                d.add_parameter("@Out2In", 1);
                           MainID = Convert.ToInt32(d.exec_salar());
                                //  N1
                                if (result.N1Loop != null)
                                    foreach (var n in result.N1Loop)
                                    {
                                        d = new Database(DBEngineType.MsSql);
                                        d.con_string =c_str;
                                        d.cmd_type = CommandType.StoredProcedure;

                                        var EntityIdentifierCode_01 = n.N1.EntityIdentifierCode_01;
                                        var N1_Name_02 = n.N1.Name_02;
                                        var IdentificationCodeQualifier_03 = n.N1.IdentificationCodeQualifier_03;
                                        var IdentificationCode_04 = n.N1.IdentificationCode_04;
                                        d.text = "pT947H_n1";
                                        d.add_parameter("MainID", MainID);
                                        d.add_parameter("Name_02", N1_Name_02==null?"": N1_Name_02);
                                        d.add_parameter("EntityIdentifierCode_01", EntityIdentifierCode_01 == null?"": EntityIdentifierCode_01);
                                        d.add_parameter("IdentificationCodeQualifier_03", IdentificationCodeQualifier_03==null?"": IdentificationCodeQualifier_03);
                                        d.add_parameter("IdentificationCode_04", IdentificationCode_04==null?"": IdentificationCode_04);
                                        d.exec_non();
                                    }

                                //  W07
                                foreach (var w in result.W19Loop)
                                {
                                    d = new Database(DBEngineType.MsSql);
                                    d.con_string =c_str;
                                    d.cmd_type = CommandType.StoredProcedure;
                                    var QuantityorStatusAdjustmentReasonCode_01 = w.W19.QuantityorStatusAdjustmentReasonCode_01;
                                    var CreditDebitQuantity_02 = w.W19.CreditDebitQuantity_02;
                                    var UnitorBasisforMeasurementCode_03 = w.W19.UnitorBasisforMeasurementCode_03;
                                    var ProductServiceIDQualifier_05 = w.W19.ProductServiceIDQualifier_05;
                                    var ProductServiceID_06 = w.W19.ProductServiceID_06;
                                    var ProductServiceIDQualifier_07 = w.W19.ProductServiceIDQualifier_07;
                                    var ProductServiceID_08 = w.W19.ProductServiceID_08;
                                    var InventoryTransactionTypeCode_16 = w.W19.InventoryTransactionTypeCode_16;
                                    var ProductServiceIDQualifier_17 = w.W19.ProductServiceIDQualifier_17;
                                    var ProductServiceID_18 = w.W19.ProductServiceID_18;
                                
                                    
                                    d.text = "pT947H_w19";
                                    d.add_parameter("MainID", MainID);
                                    d.add_parameter("QuantityorStatusAdjustmentReasonCode_01", QuantityorStatusAdjustmentReasonCode_01==null?"": QuantityorStatusAdjustmentReasonCode_01);
                                    d.add_parameter("CreditDebitQuantity_02", CreditDebitQuantity_02==null? "": CreditDebitQuantity_02);
                                    d.add_parameter("UnitorBasisforMeasurementCode_03", UnitorBasisforMeasurementCode_03==null?"": UnitorBasisforMeasurementCode_03);
                                    d.add_parameter("ProductServiceIDQualifier_05", ProductServiceIDQualifier_05==null?"": ProductServiceIDQualifier_05);
                                    d.add_parameter("ProductServiceID_06", ProductServiceID_06==null?"": ProductServiceID_06);
                                    d.add_parameter("ProductServiceIDQualifier_07", ProductServiceIDQualifier_07==null?"": ProductServiceIDQualifier_07);
                                    d.add_parameter("ProductServiceID_08", ProductServiceID_08==null?"": ProductServiceID_08);
                                    d.add_parameter("InventoryTransactionTypeCode_16", InventoryTransactionTypeCode_16==null? "": InventoryTransactionTypeCode_16);
                                    d.add_parameter("ProductServiceIDQualifier_17", ProductServiceIDQualifier_17==null?"": ProductServiceIDQualifier_17);
                                    d.add_parameter("ProductServiceID_18", ProductServiceID_18==null?"": ProductServiceID_18);
                                    d.exec_non();

                                }

                                //  N9
                                foreach (var n in result.N9)
                                {
                                    d = new Database(DBEngineType.MsSql);
                                    d.con_string =c_str;
                                    d.cmd_type = CommandType.StoredProcedure;
                                    var ReferenceIdentificationQualifier_01 = n.ReferenceIdentificationQualifier_01;
                                    var ReferenceIdentification_02 = n.ReferenceIdentification_02;
                                    d.text = "pT947H_n9";
                                    d.add_parameter("MainID", MainID);
                                    d.add_parameter("ReferenceIdentificationQualifier_01", ReferenceIdentificationQualifier_01==null?"": ReferenceIdentificationQualifier_01);
                                    d.add_parameter("ReferenceIdentification_02", ReferenceIdentification_02==null?"": ReferenceIdentification_02); 
                                    d.exec_non();
                                }

                                d = new Database(DBEngineType.MsSql);
                                d.con_string =c_str;
                                d.cmd_type = CommandType.StoredProcedure;
                                d.text = "up947";
                                d.add_parameter("ID", MainID);
                                d.exec_non();
                                if (File.Exists(sOutboundFolder + sEdiFile))
                                {
                                 File.Move(sOutboundFolder + sEdiFile, sOutboundFolder + "\\Success\\" + sEdiFile);
                                }


                                if (result.HasErrors)
                                {
                                    Label2.BeginInvoke((new Action(() => { Label2.Text = ("Rollback format is " + format); })));
                                    d = new Database(DBEngineType.MsSql);
                                    d.con_string =c_str;
                                    d.cmd_type = CommandType.StoredProcedure;
                                    d.text = "delT947H";
                                    d.add_parameter("MainID", MainID);
                                    d.add_parameter("Fname", sEdiFile);
                                    d.exec_non();
                                    if (File.Exists(sOutboundFolder + sEdiFile))
                                    {
                                        File.Move(sOutboundFolder + sEdiFile, sOutboundFolder + "\\Error\\" + sEdiFile);
                                    }
                                }
                                else
                                {

                                    d = new Database(DBEngineType.MsSql);
                                    d.con_string =c_str;
                                    d.cmd_type = CommandType.StoredProcedure;
                                    d.text = "up947";
                                    d.add_parameter("ID", MainID);
                                    d.exec_non();

                                    if (File.Exists(sOutboundFolder + sEdiFile))
                                    {
                                        File.Move(sOutboundFolder + sEdiFile, sOutboundFolder + "\\Success\\" + sEdiFile);
                                    }

                                }
                            }
                            catch(Exception x)
                            {

                                Label2.BeginInvoke((new Action(() => { Label2.Text = ("Rollback format is " + format); })));
                                  d = new Database(DBEngineType.MsSql);
                                d.con_string =c_str;
                                d.cmd_type = CommandType.StoredProcedure;
                                d.text = "delT947H";
                                d.add_parameter("MainID", MainID);
                                d.add_parameter("Msj", x.Message.ToString());

                                d.add_parameter("Fname", sEdiFile);
                                d.exec_non();
                                if (File.Exists(sOutboundFolder + sEdiFile))
                                {
                                    File.Move(sOutboundFolder + sEdiFile, sOutboundFolder + "\\Error\\" + sEdiFile);
                                }
                                t.Start();

                            }
                        }

                    }

                  


                    System.Threading.Thread.Sleep(1);
                }
            }
            t.Start();

        }

        private void cmdSend_Click(object sender, EventArgs e)
        {

            if (t == null)
                t = new System.Timers.Timer();
            t.Interval = 5000;
            t.Elapsed += T_Elapsed;
            if (cmdSend.Text == "BAŞLAT")
            {
                t.Start();
                cmdSend.Text = "DURDUR";
                cmdSend.Refresh();

            }
            else
            {
                t.Stop();
                cmdSend.Text = "BAŞLAT";
                cmdSend.Refresh();


            }
        }

        private void cmdClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
    
}
