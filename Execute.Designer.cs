﻿namespace EdiFabric.TranslateX12.Net45
{
    partial class Execute
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdClose = new System.Windows.Forms.Button();
            this.cmdSend = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.btnWriteDI = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cmdClose
            // 
            this.cmdClose.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.cmdClose.Location = new System.Drawing.Point(381, 161);
            this.cmdClose.Name = "cmdClose";
            this.cmdClose.Size = new System.Drawing.Size(154, 51);
            this.cmdClose.TabIndex = 88;
            this.cmdClose.Text = "KAPAT";
            this.cmdClose.Click += new System.EventHandler(this.cmdClose_Click);
            // 
            // cmdSend
            // 
            this.cmdSend.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.cmdSend.Location = new System.Drawing.Point(173, 161);
            this.cmdSend.Name = "cmdSend";
            this.cmdSend.Size = new System.Drawing.Size(154, 51);
            this.cmdSend.TabIndex = 87;
            this.cmdSend.Text = "BAŞLAT";
            this.cmdSend.Click += new System.EventHandler(this.cmdSend_Click);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label3.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.label3.Location = new System.Drawing.Point(8, 133);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(696, 25);
            this.label3.TabIndex = 85;
            this.label3.Text = "NETAX AS2 FILE EXPORTER";
            // 
            // Label2
            // 
            this.Label2.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.Label2.Location = new System.Drawing.Point(10, 29);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(696, 56);
            this.Label2.TabIndex = 86;
            this.Label2.Text = "NETAX AS2 ";
            // 
            // btnWriteDI
            // 
            this.btnWriteDI.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.btnWriteDI.Location = new System.Drawing.Point(289, 55);
            this.btnWriteDI.Name = "btnWriteDI";
            this.btnWriteDI.Size = new System.Drawing.Size(154, 51);
            this.btnWriteDI.TabIndex = 89;
            this.btnWriteDI.Text = "BAŞLAT ex";
            this.btnWriteDI.Click += new System.EventHandler(this.btnWriteDI_Click);
            // 
            // Execute
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(701, 241);
            this.Controls.Add(this.btnWriteDI);
            this.Controls.Add(this.cmdClose);
            this.Controls.Add(this.cmdSend);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Label2);
            this.Name = "Execute";
            this.Text = "File Writer";
            this.Load += new System.EventHandler(this.Execute_Load);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Button cmdClose;
        internal System.Windows.Forms.Button cmdSend;
        internal System.Windows.Forms.Label label3;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Button btnWriteDI;
    }
}