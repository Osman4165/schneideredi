﻿using EdiFabric;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace EDISchenider
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            SerialKey.Set("BQHL4-XCBVH-LUFAS-NLIMC-D44GQ-ZNED2-EGSZD-ETJO5-CUKLU-YJT4C-AFKMR-CED");
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new EDIWriter()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
