﻿using DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using EdiFabric.Core;
using EdiFabric.Framework.Readers;
using EdiFabric.Templates.X12004010;
using EdiFabric.Framework.Writers;
using EdiFabric.Core.Model.Edi.X12;
using EdiFabric.Framework;
using System.IO;

namespace EDISchenider
{
    public partial class EDIWriter : ServiceBase
    {
        Timer timer = new Timer();
        private string con_str;
        public EDIWriter()
        {
            InitializeComponent();
            con_str = ConfigurationManager.AppSettings["con_str"];
        }

        protected override void OnStart(string[] args)
        {
            timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);
            timer.Interval = TimeSpan.FromMinutes(10).TotalMilliseconds;
            timer.Enabled = true;
            LoggerToFile("Service is started -- " + DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"));
        }
        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {
            LoggerToFile("Call WriteEDI Method -- " + DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"));
            WriteEDI();
        }
        protected override void OnStop()
        {
            LoggerToFile("Service is Stopped -- " + DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"));
        }

        public void WriteEDI()
        {
            string sOutboundFolder = ConfigurationManager.AppSettings["outboundDirectory"];

            {
                Database d = new Database(DBEngineType.MsSql);
                d.con_string = con_str;
                d.cmd_type = CommandType.StoredProcedure;

                {

                    String format = "";
                    System.Threading.Thread.Sleep(1);

                    LoggerToFile("Call EDI846 Method -- " + DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"));
                    int MainID;
                    EDI846(sOutboundFolder, out d, out format, out MainID);

                    LoggerToFile("Call EDI856d Method -- " + DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"));
                    MainID = EDI856d(sOutboundFolder, out d, out format, MainID);

                    LoggerToFile("Call EDI856s Method -- " + DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"));
                    MainID = EDI856s(sOutboundFolder, out d, out format, MainID);

                    LoggerToFile("Call EDI944 Method -- " + DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"));
                    EDI944(sOutboundFolder, out d, out format, out MainID);

                    LoggerToFile("Call EDI947 Method -- " + DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"));
                    EDI947(sOutboundFolder, out d, out format, out MainID);


                }



                System.Threading.Thread.Sleep(1);
            }

        }

        private void EDI947(string sOutboundFolder, out Database d, out string format, out int MainID)
        {
            format = "EDI 947";
            MainID = 0;

            d = new Database(DBEngineType.MsSql);
            d.con_string = con_str;

            //  Indicates the start of a purchase order transaction set and assigns a control number.
            d.text = "select * from T947H   with(nolock) where  Updated=0 and In2Out=1  and IsOk=1  ";

            DataTable dt = d.table;
            foreach (DataRow r in dt.Rows)
            {
                var result = new TS947();
                try
                {
                    X12WriterSettings xs = new X12WriterSettings();
                    xs.PreserveWhitespace = true;
                    xs.AutoTrailers = true;
                    xs.Encoding = Encoding.ASCII;
                    xs.Separators.Segment = Separators.X12.Segment;
                    Database dnm = new Database(DBEngineType.MsSql);
                    dnm.con_string = con_str;

                    //  Indicates the start of a purchase order transaction set and assigns a control number.
                    dnm.text = "select top 1 Name_02 from T947H_n1  with(nolock) where  MainID=" + r["ID"].ToString();
                    //  REF
                    DataTable rnm = dnm.table;
                    String nm1 = rnm.Rows[0][0].ToString();
                    String fileName = sOutboundFolder + @"\3PL_" + nm1 + "_947_" + r["AdjustmentNumber_02"] + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".txt";
                    using (var writer = new X12Writer(fileName, false, xs))
                    {



                        //   writer.Write(SegmentBuilders.BuildIsa(r["AdjustmentNumber_02"].ToString(), "SSETRPRD ", "ZZ", "SCHE3PLTR  ", "ZZ","0", "P"));

                        // writer.Write(SegmentBuilders.BuildGs(r["AdjustmentNumber_02"].ToString(), "SSETRPRD ", "SCHE3PLTR  "));


                        var isa = new ISA();
                        isa.InterchangeControlVersionNumber_12 = "00402";
                        isa.InterchangeControlNumber_13 = (Convert.ToInt32(r["ID"]) + 100000000).ToString();
                        isa.AcknowledgementRequested_14 = "0";
                        isa.SecurityInformationQualifier_3 = "00";
                        isa.SecurityInformation_4 = "          ";
                        isa.AuthorizationInformation_2 = "          ";
                        isa.SenderIDQualifier_5 = "ZZ";
                        isa.InterchangeSenderID_6 = "SSETRPRD       ";
                        isa.ReceiverIDQualifier_7 = "ZZ";
                        isa.InterchangeReceiverID_8 = "SCHE3PLTR      ";
                        isa.InterchangeDate_9 = DateTime.Now.ToString("yyMMdd");
                        isa.InterchangeTime_10 = DateTime.Now.ToString("HHmm");
                        isa.UsageIndicator_15 = "P";
                        isa.AuthorizationInformationQualifier_1 = "00";
                        isa.InterchangeControlStandardsIdentifier_11 = "^";
                        //writer.Write(SegmentBuilders.BuildIsa(r["TransactionSetPurposeCode_01"].ToString(), "SSETRPRD ", "ZZ", "SCHE3PLTR  ", "ZZ", "0", "P"));
                        writer.Write(isa);
                        //writer.Write(SegmentBuilders.BuildGs(r["TransactionSetPurposeCode_01"].ToString(), "SSETRPRD ", "SCHE3PLTR  "));
                        var gs = new GS();
                        gs.CodeIdentifyingInformationType_1 = "RE";
                        gs.SenderIDCode_2 = "SSETRPRD ";
                        gs.ReceiverIDCode_3 = "SCHE3PLTR  ";
                        gs.Date_4 = DateTime.Now.ToString("yyyyMMdd");
                        gs.Time_5 = DateTime.Now.ToString("HHmm");
                        gs.GroupControlNumber_6 = (Convert.ToInt32(r["ID"]) + 100000000).ToString();
                        gs.TransactionTypeCode_7 = "X";
                        gs.VersionAndRelease_8 = "004010";

                        writer.Write(gs);
                        result.ST = new ST();
                        result.ST.TransactionSetIdentifierCode_01 = "947";
                        result.ST.TransactionSetControlNumber_02 = "000000001";
                        result.W15 = new W15();

                        result.W15.Date_01 = (r["Date_01"] == null ? "" : r["Date_01"].ToString());
                        result.W15.AdjustmentNumber_02 = (r["AdjustmentNumber_02"] == null ? "" : r["AdjustmentNumber_02"].ToString());
                        result.W15.AdjustmentNumber_03 = (r["AdjustmentNumber_03"] == null ? "" : r["AdjustmentNumber_03"].ToString());

                        MainID = Convert.ToInt32(r["ID"]);


                        d = new Database(DBEngineType.MsSql);
                        d.con_string = con_str;

                        //  Indicates the start of a purchase order transaction set and assigns a control number.
                        d.text = "select * from T947H_n1  with(nolock) where  MainID=" + MainID;
                        //  REF
                        DataTable re = d.table;
                        result.N1Loop = new List<Loop_N1_947>();
                        foreach (DataRow rw in re.Rows)
                        {

                            var n1 = new Loop_N1_947();
                            n1.N1 = new N1();

                            n1.N1.EntityIdentifierCode_01 = (rw["EntityIdentifierCode_01"] == null ? "" : rw["EntityIdentifierCode_01"].ToString());
                            n1.N1.Name_02 = (rw["Name_02"] == null ? "" : rw["Name_02"].ToString());
                            n1.N1.IdentificationCodeQualifier_03 = (rw["IdentificationCodeQualifier_03"] == null ? "" : rw["IdentificationCodeQualifier_03"].ToString());
                            n1.N1.IdentificationCode_04 = (rw["IdentificationCode_04"] == null ? "" : rw["IdentificationCode_04"].ToString());

                            result.N1Loop.Add(n1);
                        }

                        d = new Database(DBEngineType.MsSql);
                        d.con_string = con_str;

                        //  Indicates the start of a purchase order transaction set and assigns a control number.
                        d.text = "select * from T947H_w07  with(nolock) where  MainID=" + MainID;
                        //  REF
                        re = d.table;
                        result.W19Loop = new List<Loop_W19_947>();
                        foreach (DataRow rw in re.Rows)
                        {

                            var w19 = new Loop_W19_947();
                            w19.W19 = new W19();

                            w19.W19.QuantityorStatusAdjustmentReasonCode_01 = (rw["QuantityorStatusAdjustmentReasonCode_01"] == null ? "" : rw["QuantityorStatusAdjustmentReasonCode_01"].ToString());
                            w19.W19.CreditDebitQuantity_02 = (rw["CreditDebitQuantity_02"] == null ? "" : rw["CreditDebitQuantity_02"].ToString());
                            w19.W19.UnitorBasisforMeasurementCode_03 = (rw["UnitorBasisforMeasurementCode_03"] == null ? "" : rw["UnitorBasisforMeasurementCode_03"].ToString());
                            w19.W19.ProductServiceIDQualifier_05 = (rw["ProductServiceIDQualifier_05"] == null ? "" : rw["ProductServiceIDQualifier_05"].ToString());
                            w19.W19.ProductServiceID_06 = (rw["ProductServiceID_06"] == null ? "" : rw["ProductServiceID_06"].ToString());
                            w19.W19.ProductServiceIDQualifier_07 = (rw["ProductServiceIDQualifier_07"] == null ? "" : rw["ProductServiceIDQualifier_07"].ToString());
                            w19.W19.ProductServiceID_08 = (rw["ProductServiceID_08"] == null ? "" : rw["ProductServiceID_08"].ToString());
                            w19.W19.InventoryTransactionTypeCode_16 = (rw["InventoryTransactionTypeCode_16"] == null ? "" : rw["InventoryTransactionTypeCode_16"].ToString());
                            w19.W19.ProductServiceIDQualifier_17 = (rw["ProductServiceIDQualifier_17"] == null ? "" : rw["ProductServiceIDQualifier_17"].ToString());
                            w19.W19.ProductServiceID_18 = (rw["ProductServiceID_18"] == null ? "" : rw["ProductServiceID_18"].ToString());

                            result.W19Loop.Add(w19);
                        }


                        d = new Database(DBEngineType.MsSql);
                        d.con_string = con_str;

                        //  Indicates the start of a purchase order transaction set and assigns a control number.
                        d.text = "select * from T947H_n9  with(nolock) where  MainID=" + MainID;
                        //  REF
                        re = d.table; result.N9 = new List<N9>();
                        foreach (DataRow rw in re.Rows)
                        {


                            var n9 = new N9();

                            n9.ReferenceIdentificationQualifier_01 = (rw["ReferenceIdentificationQualifier_01"] == null ? "" : rw["ReferenceIdentificationQualifier_01"].ToString());
                            n9.ReferenceIdentification_02 = (rw["ReferenceIdentification_02"] == null ? "" : rw["ReferenceIdentification_02"].ToString());
                            n9.ReferenceIdentification_02 = (rw["ReferenceIdentification_02"] == null ? "" : rw["ReferenceIdentification_02"].ToString());

                            result.N9.Add(n9);
                        }

                        d = new Database(DBEngineType.MsSql);
                        d.con_string = con_str;

                        //  Indicates the start of a purchase order transaction set and assigns a control number.
                        d.text = "select * from T947H_w07  with(nolock) where  MainID=" + MainID;
                        //  REF
                        re = d.table;

                        if (result.HasErrors)
                        {
                            //Error Log
                            d = new Database(DBEngineType.MsSql);
                            d.con_string = con_str;

                            //  Indicates the start of a purchase order transaction set and assigns a control number.
                            d.text = "insert into TLogs (MainID,Tablo, Mesaj) values(" + MainID + ",'T947H','" + result.ErrorContext.ToString() + "')";
                            d.exec_non();

                        }
                        else
                        {
                            //Update DB and flag

                            writer.Write(result);
                            writer.Flush();
                            writer.Dispose();
                            string str = File.ReadAllText(fileName);
                            if (str.IndexOf("~") > 10)
                            {
                                str = str.Replace("~", "~\r\n");
                                //str = str.Replace("*U*", "*^*");
                                //str = str.Replace("GS*IN*", "GS*RE*");
                                str = str.Replace("*00204*", "*00402*");
                                File.WriteAllText(fileName, str);
                            }
                            d = new Database(DBEngineType.MsSql);
                            d.con_string = con_str;

                            //  Indicates the start of a purchase order transaction set and assigns a control number.
                            d.text = "update T947H  set Updated=1  where  ID=" + MainID;
                            d.exec_non();
                        }



                    }





                }
                catch (Exception x)
                {
                    LoggerToFile("Rollback format is -- " + format + " -- " + DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"));
                    //Error Log
                    d = new Database(DBEngineType.MsSql);
                    d.con_string = con_str;

                    //  Indicates the start of a purchase order transaction set and assigns a control number.
                    d.text = "insert into TLogs (MainID,Tablo, Mesaj) values(" + MainID + ",'T947H','" + x.ToString() + "')";
                    d.exec_non();
                }


            }

        }

        private void EDI944(string sOutboundFolder, out Database d, out string format, out int MainID)
        {
            format = "EDI 944";
            MainID = 0;

            d = new Database(DBEngineType.MsSql);
            d.con_string = con_str;

            //  Indicates the start of a purchase order transaction set and assigns a control number.
            d.text = "select * from T944H   with(nolock) where   Updated=0 and In2Out=1  and IsOk=1 ";

            DataTable dt = d.table;
            foreach (DataRow r in dt.Rows)
            {
                var result = new TS944();

                try
                {
                    X12WriterSettings xs = new X12WriterSettings();
                    xs.PreserveWhitespace = true;
                    xs.AutoTrailers = true;
                    xs.PreserveWhitespace = true;
                    xs.Encoding = Encoding.ASCII;
                    Database dnm = new Database(DBEngineType.MsSql);
                    dnm.con_string = con_str;

                    //  Indicates the start of a purchase order transaction set and assigns a control number.
                    dnm.text = "select top 1 Name_02 from T944H_n1  with(nolock) where  MainID=" + r["ID"].ToString();
                    //  REF
                    DataTable rnm = dnm.table;
                    String nm1 = rnm.Rows[0][0].ToString();
                    String fileName = sOutboundFolder + @"\3PL_" + nm1 + "_944_" + r["ShipmentIdentificationNumber_05"] + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".txt";
                    using (var writer = new X12Writer(fileName, false, xs))
                    {
                        //writer.Write(SegmentBuilders.BuildIsa(r["ShipmentIdentificationNumber_05"].ToString(),"SSETRPRD ", "ZZ", "SCHE3PLTR  ", "ZZ", "0", "P"));

                        //     writer.Write(SegmentBuilders.BuildGs(r["ShipmentIdentificationNumber_05"].ToString(), "SSETRPRD ", "SCHE3PLTR  "));

                        var isa = new ISA();
                        isa.InterchangeControlVersionNumber_12 = "00402";
                        isa.InterchangeControlNumber_13 = (Convert.ToInt32(r["ID"]) + 100000000).ToString();
                        isa.AcknowledgementRequested_14 = "0";
                        isa.SecurityInformationQualifier_3 = "00";
                        isa.SecurityInformation_4 = "          ";
                        isa.AuthorizationInformation_2 = "          ";
                        isa.SenderIDQualifier_5 = "ZZ";
                        isa.InterchangeSenderID_6 = "SSETRPRD       ";
                        isa.ReceiverIDQualifier_7 = "ZZ";
                        isa.InterchangeReceiverID_8 = "SCHE3PLTR      ";
                        isa.InterchangeDate_9 = DateTime.Now.ToString("yyMMdd");
                        isa.InterchangeTime_10 = DateTime.Now.ToString("HHmm");
                        isa.UsageIndicator_15 = "P";
                        isa.AuthorizationInformationQualifier_1 = "00";
                        isa.InterchangeControlStandardsIdentifier_11 = "^";
                        //writer.Write(SegmentBuilders.BuildIsa(r["TransactionSetPurposeCode_01"].ToString(), "SSETRPRD ", "ZZ", "SCHE3PLTR  ", "ZZ", "0", "P"));
                        writer.Write(isa);
                        //writer.Write(SegmentBuilders.BuildGs(r["TransactionSetPurposeCode_01"].ToString(), "SSETRPRD ", "SCHE3PLTR  "));
                        var gs = new GS();
                        gs.CodeIdentifyingInformationType_1 = "RE";
                        gs.SenderIDCode_2 = "SSETRPRD ";
                        gs.ReceiverIDCode_3 = "SCHE3PLTR  ";
                        gs.Date_4 = DateTime.Now.ToString("yyyyMMdd");
                        gs.Time_5 = DateTime.Now.ToString("HHmm");
                        gs.GroupControlNumber_6 = (Convert.ToInt32(r["ID"]) + 100000000).ToString();
                        gs.TransactionTypeCode_7 = "X";
                        gs.VersionAndRelease_8 = "004010";
                        writer.Write(gs);
                        result.W17 = new W17();
                        result.ST = new ST();
                        result.ST.TransactionSetIdentifierCode_01 = "944";
                        result.ST.TransactionSetControlNumber_02 = "000000001";

                        result.W17.ReportingCode_01 = (r["ReportingCode_01"] == null ? "" : r["ReportingCode_01"].ToString());
                        result.W17.Date_02 = (r["Date_02"] == null ? "" : r["Date_02"].ToString());
                        result.W17.WarehouseReceiptNumber_03 = (r["WarehouseReceiptNumber_03"] == null ? "" : r["WarehouseReceiptNumber_03"].ToString());
                        result.W17.ShipmentIdentificationNumber_05 = (r["ShipmentIdentificationNumber_05"] == null ? "" : r["ShipmentIdentificationNumber_05"].ToString());
                        result.W17.TimeQualifier_06 = (r["TimeQualifier_06"] == null ? "" : r["TimeQualifier_06"].ToString());
                        result.W17.Time_07 = (r["Time_07"] == null ? "" : r["Time_07"].ToString());
                        result.W14 = new W14();
                        result.W14.QuantityReceived_01 = (r["w14QuantityReceived_01"] == null ? "" : r["w14QuantityReceived_01"].ToString());

                        MainID = Convert.ToInt32(r["ID"]);


                        d = new Database(DBEngineType.MsSql);
                        d.con_string = con_str;

                        //  Indicates the start of a purchase order transaction set and assigns a control number.
                        d.text = "select * from T944H_n1  with(nolock) where  MainID=" + MainID;
                        //  REF
                        DataTable re = d.table;
                        result.N1Loop = new List<Loop_N1_944>();
                        foreach (DataRow rw in re.Rows)
                        {

                            var n1 = new Loop_N1_944();
                            n1.N1 = new N1();

                            n1.N1.EntityIdentifierCode_01 = (rw["EntityIdentifierCode_01"] == null ? "" : rw["EntityIdentifierCode_01"].ToString());
                            n1.N1.Name_02 = (rw["Name_02"] == null ? "" : rw["Name_02"].ToString());
                            n1.N1.IdentificationCodeQualifier_03 = (rw["IdentificationCodeQualifier_03"] == null ? "" : rw["IdentificationCodeQualifier_03"].ToString());
                            n1.N1.IdentificationCode_04 = (rw["IdentificationCode_04"] == null ? "" : rw["IdentificationCode_04"].ToString());

                            result.N1Loop.Add(n1);
                        }


                        d = new Database(DBEngineType.MsSql);
                        d.con_string = con_str;

                        //  Indicates the start of a purchase order transaction set and assigns a control number.
                        d.text = "select * from T944H_n9  with(nolock) where  MainID=" + MainID;
                        //  REF
                        re = d.table; result.N9 = new List<N9>();
                        foreach (DataRow rw in re.Rows)
                        {


                            var n9 = new N9();

                            n9.ReferenceIdentificationQualifier_01 = (rw["ReferenceIdentificationQualifier_01"] == null ? "" : rw["ReferenceIdentificationQualifier_01"].ToString());
                            n9.ReferenceIdentification_02 = (rw["ReferenceIdentification_02"] == null ? "" : rw["ReferenceIdentification_02"].ToString());
                            n9.ReferenceIdentification_02 = (rw["ReferenceIdentification_02"] == null ? "" : rw["ReferenceIdentification_02"].ToString());

                            result.N9.Add(n9);
                        }

                        d = new Database(DBEngineType.MsSql);
                        d.con_string = con_str;

                        //  Indicates the start of a purchase order transaction set and assigns a control number.
                        d.text = "select * from T944H_w07  with(nolock) where   MainID=" + MainID;
                        //  REF
                        re = d.table;
                        result.W07Loop = new List<Loop_W07_944>();
                        foreach (DataRow rw in re.Rows)
                        {


                            var w07 = new Loop_W07_944();
                            w07.W07 = new W07();
                            w07.W07.QuantityReceived_01 = (rw["wQuantityReceived_01"] == null ? "" : rw["wQuantityReceived_01"].ToString());
                            w07.W07.UnitorBasisforMeasurementCode_02 = (rw["UnitorBasisforMeasurementCode_02"] == null ? "" : rw["UnitorBasisforMeasurementCode_02"].ToString());
                            w07.W07.ProductServiceIDQualifier_04 = (rw["ProductServiceIDQualifier_04"] == null ? "" : rw["ProductServiceIDQualifier_04"].ToString());
                            w07.W07.ProductServiceID_05 = (rw["ProductServiceID_05"] == null ? "" : rw["ProductServiceID_05"].ToString());
                            w07.W07.ProductServiceIDQualifier_06 = (rw["ProductServiceIDQualifier_06"] == null ? "" : rw["ProductServiceIDQualifier_06"].ToString());
                            w07.W07.ProductServiceID_07 = (rw["ProductServiceID_07"] == null ? "" : rw["ProductServiceID_07"].ToString());
                            w07.W07.ProductServiceIDQualifier_10 = (rw["ProductServiceIDQualifier_10"] == null ? "" : rw["ProductServiceIDQualifier_10"].ToString());
                            w07.W07.ProductServiceID_11 = (rw["ProductServiceID_11"] == null ? "" : rw["ProductServiceID_11"].ToString());


                            result.W07Loop.Add(w07);
                        }



                        if (result.HasErrors)
                        {
                            //Error Log
                            d = new Database(DBEngineType.MsSql);
                            d.con_string = con_str;

                            //  Indicates the start of a purchase order transaction set and assigns a control number.
                            d.text = "insert into TLogs (MainID,Tablo, Mesaj) values(" + MainID + ",'T944H','" + result.ErrorContext.ToString() + "')";
                            d.exec_non();

                        }
                        else
                        {
                            //Update DB and flag

                            writer.Write(result);
                            writer.Flush();
                            writer.Dispose();
                            string str = File.ReadAllText(fileName);
                            if (str.IndexOf("~") > 10)
                            {
                                str = str.Replace("~", "~\r\n");
                                //str = str.Replace("*U*", "*^*");
                                //str = str.Replace("GS*IN*", "GS*RE*");
                                str = str.Replace("*00204*", "*00402*");
                                File.WriteAllText(fileName, str);
                            }
                            d = new Database(DBEngineType.MsSql);
                            d.con_string = con_str;

                            //  Indicates the start of a purchase order transaction set and assigns a control number.
                            d.text = "update T944H  set Updated=1  where  ID=" + MainID;
                            d.exec_non();
                        }



                    }





                }
                catch (Exception x)
                {
                    LoggerToFile("Rollback format is -- " + format + " -- " + DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"));
                    d = new Database(DBEngineType.MsSql);
                    d.con_string = con_str;

                    //  Indicates the start of a purchase order transaction set and assigns a control number.
                    d.text = "insert into TLogs (MainID,Tablo, Mesaj) values(" + MainID + ",'T944H','" + x.ToString() + "')";
                    d.exec_non();
                    //Label2.BeginInvoke((new Action(() => { Label2.Text = ("Rollback format is " + format); })));
                    //d = new Database(DBEngineType.MsSql);
                    //d.con_string = con_str;
                    //d.cmd_type = CommandType.StoredProcedure;
                    //d.text = "delT846H";
                    //d.add_parameter("Msj", x.Message);
                    //d.add_parameter("MainID", MainID);
                    //d.add_parameter("Fname", sEdiFile);

                    //d.exec_non();
                    //if (File.Exists(sOutboundFolder + sEdiFile))
                    //{
                    //    File.Move(sOutboundFolder + sEdiFile, sOutboundFolder + "\\Error\\" + sEdiFile);
                    //}
                }


            }

        }

        private int EDI856s(string sOutboundFolder, out Database d, out string format, int MainID)
        {
            {




                //Sorunun olduğu blok
                // REF tagı ile ilgili sorun olma ihtimali var
                format = "EDI 856s";

                d = new Database(DBEngineType.MsSql);
                d.con_string = con_str;

                //  Indicates the start of a purchase order transaction set and assigns a control number.
                d.text = "select * from T856H  with(nolock) where Updated=0 and In2Out=1 and Kind='s'  and IsOk=1 ";

                DataTable dt = d.table;
                foreach (DataRow r in dt.Rows)
                {

                    try
                    {
                        var result = new TS856();
                        X12WriterSettings xs = new X12WriterSettings();
                        xs.PreserveWhitespace = true;
                        xs.AutoTrailers = true;
                        xs.Encoding = Encoding.ASCII;
                        xs.Separators.Segment = Separators.X12.Segment;
                        Database dnm = new Database(DBEngineType.MsSql);
                        dnm.con_string = con_str;

                        //  Indicates the start of a purchase order transaction set and assigns a control number.
                        dnm.text = "select top 1 Name_02 from T856H_n  with(nolock) where  MainID=" + r["ID"].ToString();
                        //  REF
                        DataTable rnm = dnm.table;
                        string n1 = rnm.Rows[0][0].ToString();
                        String fileName = sOutboundFolder + @"\3PL_" + n1 + "_856s_" + r["ShipmentIdentification_02"] + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".txt";
                        using (var writer = new X12Writer(fileName, false, xs))

                        {


                            //writer.Write(SegmentBuilders.BuildIsa(r["TransactionSetPurposeCode_01"].ToString(), "SSETRPRD ",
                            //    "ZZ", "SCHE3PLTR  ", "ZZ", "0", "P"));

                            //writer.Write(SegmentBuilders.BuildGs(r["TransactionSetPurposeCode_01"].ToString(), "SSETRPRD ", "SCHE3PLTR  "));
                            var isa = new ISA();
                            isa.InterchangeControlVersionNumber_12 = "00402";
                            isa.InterchangeControlNumber_13 = (Convert.ToInt32(r["ID"]) + 100000000).ToString();
                            isa.AcknowledgementRequested_14 = "0";
                            isa.SecurityInformationQualifier_3 = "00";
                            isa.SecurityInformation_4 = "          ";
                            isa.AuthorizationInformation_2 = "          ";
                            isa.SenderIDQualifier_5 = "ZZ";
                            isa.InterchangeSenderID_6 = "SSETRPRD       ";
                            isa.ReceiverIDQualifier_7 = "ZZ";
                            isa.InterchangeReceiverID_8 = "SCHE3PLTR      ";
                            isa.InterchangeDate_9 = DateTime.Now.ToString("yyMMdd");
                            isa.InterchangeTime_10 = DateTime.Now.ToString("HHmm");
                            isa.UsageIndicator_15 = "P";
                            isa.AuthorizationInformationQualifier_1 = "00";
                            isa.InterchangeControlStandardsIdentifier_11 = "^";
                            //writer.Write(SegmentBuilders.BuildIsa(r["TransactionSetPurposeCode_01"].ToString(), "SSETRPRD ", "ZZ", "SCHE3PLTR  ", "ZZ", "0", "P"));
                            writer.Write(isa);
                            //writer.Write(SegmentBuilders.BuildGs(r["TransactionSetPurposeCode_01"].ToString(), "SSETRPRD ", "SCHE3PLTR  "));
                            var gs = new GS();
                            gs.CodeIdentifyingInformationType_1 = "RE";
                            gs.SenderIDCode_2 = "SSETRPRD ";
                            gs.ReceiverIDCode_3 = "SCHE3PLTR  ";
                            gs.Date_4 = DateTime.Now.ToString("yyyyMMdd");
                            gs.Time_5 = DateTime.Now.ToString("HHmm");
                            gs.GroupControlNumber_6 = (Convert.ToInt32(r["ID"]) + 100000000).ToString();
                            gs.TransactionTypeCode_7 = "X";
                            gs.VersionAndRelease_8 = "004010";

                            writer.Write(gs);

                            result.BSN = new BSN();
                            result.BSN.TransactionSetPurposeCode_01 = (r["TransactionSetPurposeCode_01"] == null ? "" : r["TransactionSetPurposeCode_01"].ToString());
                            result.BSN.ShipmentIdentification_02 = (r["ShipmentIdentification_02"] == null ? "" : r["ShipmentIdentification_02"].ToString());
                            result.BSN.Date_03 = (r["Date_03"] == null ? "" : r["Date_03"].ToString());
                            result.BSN.Time_04 = r["Time_04"].ToString();
                            result.BSN.TransactionTypeCode_06 = "SH";
                            result.ST = new ST();
                            result.ST.TransactionSetIdentifierCode_01 = "856";
                            result.ST.TransactionSetControlNumber_02 = "000000001";


                            MainID = Convert.ToInt32(r["ID"]);


                            d = new Database(DBEngineType.MsSql);
                            d.con_string = con_str;

                            //  
                            d.text = "select * from T856H_hl_main with(nolock) where  MainID=" + MainID + "";
                            //  HL Main
                            DataTable re = d.table;
                            var hlMain = new List<Loop_HL_856>();
                            foreach (DataRow rw in re.Rows)
                            {
                                int HLMainID = Convert.ToInt32(rw["ID"]);

                                var hlMainLoop = new Loop_HL_856();
                                hlMainLoop.HL = new HL();
                                var hlm = hlMainLoop.HL;
                                hlm.HierarchicalIDNumber_01 = (rw["HierarchicalIDNumber_01"] == null ? "" :
                                    rw["HierarchicalIDNumber_01"].ToString());
                                hlm.HierarchicalLevelCode_03 = (rw["HierarchicalLevelCode_03"] == null ? "" : rw["HierarchicalLevelCode_03"].ToString());

                                hlm.HierarchicalChildCode_04 = (rw["HierarchicalChildCode_04"] == null ? "" : rw["HierarchicalChildCode_04"].ToString());
                                hlMainLoop.HL = hlm;


                                //  TD5
                                d = new Database(DBEngineType.MsSql);
                                d.con_string = con_str;

                                d.text = "select * from T856H_td5  with(nolock) where  MainID=" + MainID;

                                re = d.table;
                                if (re.Rows.Count > 0)
                                    hlMainLoop.TD5 = new List<TD5>();
                                foreach (DataRow lrw in re.Rows)
                                {

                                    var rf = new TD5();

                                    rf.Routing_05 = lrw["Routing_05"] == null ? "" : lrw["Routing_05"].ToString();
                                    rf.ServiceLevelCode_12 = lrw["ServiceLevelCode_12"] == null ? "" : lrw["ServiceLevelCode_12"].ToString();
                                    rf.CountryCode_15 = lrw["CountryCode_15"] == null ? "" : lrw["CountryCode_15"].ToString();
                                    hlMainLoop.TD5.Add(rf);


                                }

                                //  TD3
                                d = new Database(DBEngineType.MsSql);
                                d.con_string = con_str;

                                d.text = "select * from T856H_td3  with(nolock) where  MainID=" + MainID + " and HLMainID=" + HLMainID;

                                re = d.table;
                                if (re.Rows.Count > 0)
                                    hlMainLoop.TD3 = new List<TD3>();
                                foreach (DataRow lrw in re.Rows)
                                {

                                    var rf = new TD3();

                                    rf.EquipmentDescriptionCode_01 = lrw["EquipmentDescriptionCode_01"] == null ? "" : lrw["EquipmentDescriptionCode_01"].ToString();
                                    rf.EquipmentNumber_03 = lrw["EquipmentNumber_03"] == null ? "" : lrw["EquipmentNumber_03"].ToString();
                                    rf.SealNumber_09 = lrw["SealNumber_09"] == null ? "" : lrw["SealNumber_09"].ToString();
                                    hlMainLoop.TD3.Add(rf);


                                }

                                //  REF
                                d = new Database(DBEngineType.MsSql);
                                d.con_string = con_str;

                                d.text = "select * from T856H_ref  with(nolock) where  MainID=" + MainID;

                                re = d.table;
                                if (re.Rows.Count > 0) hlMainLoop.REF = new List<REF>();
                                foreach (DataRow lrw in re.Rows)
                                {

                                    var rf = new REF();

                                    rf.ReferenceIdentificationQualifier_01 = lrw["ReferenceIdentificationQualifier_01"] == null ? "" : lrw["ReferenceIdentificationQualifier_01"].ToString();
                                    rf.ReferenceIdentification_02 = lrw["ReferenceIdentification_02"] == null ? "" : lrw["ReferenceIdentification_02"].ToString();
                                    hlMainLoop.REF.Add(rf);


                                }


                                //  N1
                                d = new Database(DBEngineType.MsSql);
                                d.con_string = con_str;

                                d.text = "select * from T856H_n  with(nolock) where  MainID=" + MainID;

                                re = d.table;
                                if (re.Rows.Count > 0)
                                {
                                    var n1loop = new Loop_N1_856();
                                    hlMainLoop.N1Loop = new List<Loop_N1_856>();
                                    foreach (DataRow lrw in re.Rows)
                                    {

                                        var n = new N1();

                                        n.EntityIdentifierCode_01 = lrw["EntityIdentifierCode_01"] == null ? "" : lrw["EntityIdentifierCode_01"].ToString();
                                        n.Name_02 = lrw["Name_02"] == null ? "" : lrw["Name_02"].ToString();
                                        n.IdentificationCodeQualifier_03 = lrw["IdentificationCodeQualifier_03"] == null ? "" : lrw["IdentificationCodeQualifier_03"].ToString();
                                        n.IdentificationCode_04 = lrw["IdentificationCode_04"] == null ? "" : lrw["IdentificationCode_04"].ToString();
                                        n1loop.N1 = new N1();
                                        n1loop.N1 = n;
                                        hlMainLoop.N1Loop.Add(n1loop);


                                    }
                                }
                                result.HLLoop = new List<Loop_HL_856>();
                                result.HLLoop.Add(hlMainLoop);
                                var hlloop = new List<Loop_HL_856>();
                                d = new Database(DBEngineType.MsSql);
                                d.con_string = con_str;

                            }

                            /* sırasıyla T > P > I şeklinde olusmalı, T olmadıgı senaryoda P > I şeklinde olusmalı */

                            d.text = "select * from T856H_hl with(nolock) where  MainID=" + MainID + " ";
                            //  HL Main
                            DataTable rew = d.table;

                            // HL line
                            int j = 0;
                            int hlline = 1;

                            int t_p_control = rew.Rows.Cast<DataRow>().Any(w => w["HierarchicalLevelCode_03"].ToString() == "T") ? 1 : 2;
                            string hcode = t_p_control == 1 ? "T" : "P";
                            foreach (DataRow lrw in rew.Rows.Cast<DataRow>().Where(w => w["HierarchicalLevelCode_03"].ToString() == hcode))
                            {
                                int lHLID2 = Convert.ToInt32(lrw["ID"]);

                                if (lrw["HierarchicalLevelCode_03"].ToString() == "P")
                                {
                                    hlline = WriteContent(out d, MainID, result, out re, hlline, lrw);

                                    WriteIContent(ref d, MainID, result, ref re, ref hlline, lrw["HierarchicalIDNumber_01"].ToString(), lHLID2);
                                }
                                else
                                {
                                    hlline = WriteContent(out d, MainID, result, out re, hlline, lrw);

                                    d.text = "select * from T856H_hl with(nolock) where  MainID=" + MainID + " and HLID =" + lHLID2;
                                    DataTable rewP = d.table;
                                    foreach (DataRow item in rewP.Rows)
                                    {
                                        int lHLID3 = Convert.ToInt32(item["ID"]);
                                        hlline = WriteContent(out d, MainID, result, out re, hlline, item);

                                        WriteIContent(ref d, MainID, result, ref re, ref hlline, item["HierarchicalIDNumber_01"].ToString(), lHLID3);
                                    }
                                }




                            }



                            if (result.HasErrors)
                            {
                                //Error Log
                                d = new Database(DBEngineType.MsSql);
                                d.con_string = con_str;

                                //  Indicates the start of a purchase order transaction set and assigns a control number.
                                d.text = "insert into TLogs (MainID,Tablo, Mesaj) values(" + MainID + ",'T846H','" + result.ErrorContext.ToString() + "')";
                                d.exec_non();

                            }
                            else
                            {
                                //Update DB and flag

                                writer.Write(result);
                                writer.Flush();
                                writer.Dispose();
                                string str = File.ReadAllText(fileName);
                                if (str.IndexOf("~") > 10)
                                {
                                    str = str.Replace("~", "~\r\n");
                                    //str = str.Replace("*U*", "*^*");
                                    //str = str.Replace("GS*IN*", "GS*RE*");
                                    var i = str.LastIndexOf(@"~") + 1;
                                    str = str.Remove(i);
                                    File.WriteAllText(fileName, str);
                                }
                                d = new Database(DBEngineType.MsSql);
                                d.con_string = con_str;

                                //  Indicates the start of a purchase order transaction set and assigns a control number.
                                d.text = "update T856H  set Updated=1  where  ID=" + MainID;
                                d.exec_non();
                            }



                        }


                    }
                    catch (Exception x)
                    {
                        LoggerToFile("Rollback format is -- " + format + " -- " + DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"));
                        //Error Log
                        d = new Database(DBEngineType.MsSql);
                        d.con_string = con_str;

                        //  Indicates the start of a purchase order transaction set and assigns a control number.
                        d.text = "insert into TLogs (MainID,Tablo, Mesaj) values(" + MainID + ",'T846H','" + x.ToString() + "')";
                        d.exec_non();
                    }


                }

            }

            return MainID;
        }

        private int EDI856d(string sOutboundFolder, out Database d, out string format, int MainID)
        {
            {






                format = "EDI 856d";

                d = new Database(DBEngineType.MsSql);
                d.con_string = con_str;

                //  Indicates the start of a purchase order transaction set and assigns a control number.
                d.text = "select * from T856H   with(nolock) where   Updated=0 and In2Out=1 and Kind='d'  and IsOk=1 ";

                DataTable dt = d.table;
                foreach (DataRow r in dt.Rows)
                {

                    try
                    {
                        var result = new TS856();
                        X12WriterSettings xs = new X12WriterSettings();
                        xs.PreserveWhitespace = true;
                        xs.AutoTrailers = true;
                        xs.Encoding = Encoding.ASCII;
                        xs.Separators.Segment = Separators.X12.Segment;
                        Database dnm = new Database(DBEngineType.MsSql);
                        dnm.con_string = con_str;

                        //  Indicates the start of a purchase order transaction set and assigns a control number.
                        dnm.text = "select top 1 Name_02 from T856H_n  with(nolock) where  MainID=" + r["ID"].ToString();
                        //  REF
                        DataTable rnm = dnm.table;
                        string n1 = rnm.Rows[0][0].ToString();
                        String fileName = sOutboundFolder + @"\3PL_" + n1 + "_856d_" + r["ShipmentIdentification_02"] + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".txt";
                        using (var writer = new X12Writer(fileName, false, xs))

                        {


                            //writer.Write(SegmentBuilders.BuildIsa(r["TransactionSetPurposeCode_01"].ToString(), "SSETRPRD ",
                            //    "ZZ", "SCHE3PLTR  ", "ZZ", "0", "P"));

                            //writer.Write(SegmentBuilders.BuildGs(r["TransactionSetPurposeCode_01"].ToString(), "SSETRPRD ", "SCHE3PLTR  "));
                            var isa = new ISA();
                            isa.InterchangeControlVersionNumber_12 = "00402";
                            isa.InterchangeControlNumber_13 = (Convert.ToInt32(r["ID"]) + 100000000).ToString();
                            isa.AcknowledgementRequested_14 = "0";
                            isa.SecurityInformationQualifier_3 = "00";
                            isa.SecurityInformation_4 = "          ";
                            isa.AuthorizationInformation_2 = "          ";
                            isa.SenderIDQualifier_5 = "ZZ";
                            isa.InterchangeSenderID_6 = "SSETRPRD       ";
                            isa.ReceiverIDQualifier_7 = "ZZ";
                            isa.InterchangeReceiverID_8 = "SCHE3PLTR      ";
                            isa.InterchangeDate_9 = DateTime.Now.ToString("yyMMdd");
                            isa.InterchangeTime_10 = DateTime.Now.ToString("HHmm");
                            isa.UsageIndicator_15 = "P";
                            isa.AuthorizationInformationQualifier_1 = "00";
                            isa.InterchangeControlStandardsIdentifier_11 = "^";
                            //writer.Write(SegmentBuilders.BuildIsa(r["TransactionSetPurposeCode_01"].ToString(), "SSETRPRD ", "ZZ", "SCHE3PLTR  ", "ZZ", "0", "P"));
                            writer.Write(isa);
                            //writer.Write(SegmentBuilders.BuildGs(r["TransactionSetPurposeCode_01"].ToString(), "SSETRPRD ", "SCHE3PLTR  "));
                            var gs = new GS();
                            gs.CodeIdentifyingInformationType_1 = "RE";
                            gs.SenderIDCode_2 = "SSETRPRD ";
                            gs.ReceiverIDCode_3 = "SCHE3PLTR  ";
                            gs.Date_4 = DateTime.Now.ToString("yyyyMMdd");
                            gs.Time_5 = DateTime.Now.ToString("HHmm");
                            gs.GroupControlNumber_6 = (Convert.ToInt32(r["ID"]) + 100000000).ToString();
                            gs.TransactionTypeCode_7 = "X";
                            gs.VersionAndRelease_8 = "004010";

                            writer.Write(gs);

                            result.BSN = new BSN();
                            result.BSN.TransactionSetPurposeCode_01 = (r["TransactionSetPurposeCode_01"] == null ? "" : r["TransactionSetPurposeCode_01"].ToString());
                            result.BSN.ShipmentIdentification_02 = (r["ShipmentIdentification_02"] == null ? "" : r["ShipmentIdentification_02"].ToString());
                            result.BSN.Date_03 = (r["Date_03"] == null ? "" : r["Date_03"].ToString());
                            result.BSN.Time_04 = r["Time_04"].ToString();
                            result.BSN.TransactionTypeCode_06 = "AS";
                            result.ST = new ST();
                            result.ST.TransactionSetIdentifierCode_01 = "856";
                            result.ST.TransactionSetControlNumber_02 = "000000001";


                            MainID = Convert.ToInt32(r["ID"]);


                            d = new Database(DBEngineType.MsSql);
                            d.con_string = con_str;

                            //  
                            d.text = "select * from T856H_hl_main with(nolock) where  MainID=" + MainID + "";
                            //  HL Main
                            DataTable re = d.table;
                            var hlMain = new List<Loop_HL_856>();
                            foreach (DataRow rw in re.Rows)
                            {
                                int HLMainID = Convert.ToInt32(rw["ID"]);

                                var hlMainLoop = new Loop_HL_856();
                                hlMainLoop.HL = new HL();
                                var hlm = hlMainLoop.HL;
                                hlm.HierarchicalIDNumber_01 = (rw["HierarchicalIDNumber_01"] == null ? "" :
                                    rw["HierarchicalIDNumber_01"].ToString());
                                hlm.HierarchicalLevelCode_03 = (rw["HierarchicalLevelCode_03"] == null ? "" : rw["HierarchicalLevelCode_03"].ToString());

                                hlm.HierarchicalChildCode_04 = (rw["HierarchicalChildCode_04"] == null ? "" : rw["HierarchicalChildCode_04"].ToString());
                                hlMainLoop.HL = hlm;

                                //  REF
                                d = new Database(DBEngineType.MsSql);
                                d.con_string = con_str;

                                d.text = "select * from T856H_ref  with(nolock) where  MainID=" + MainID;

                                re = d.table;
                                if (re.Rows.Count > 0) hlMainLoop.REF = new List<REF>();
                                foreach (DataRow lrw in re.Rows)
                                {

                                    var rf = new REF();

                                    rf.ReferenceIdentificationQualifier_01 = lrw["ReferenceIdentificationQualifier_01"] == null ? "" : lrw["ReferenceIdentificationQualifier_01"].ToString();
                                    rf.ReferenceIdentification_02 = lrw["ReferenceIdentification_02"] == null ? "" : lrw["ReferenceIdentification_02"].ToString();
                                    hlMainLoop.REF.Add(rf);


                                }


                                //  N1
                                d = new Database(DBEngineType.MsSql);
                                d.con_string = con_str;

                                d.text = "select * from T856H_n  with(nolock) where  MainID=" + MainID;

                                re = d.table;
                                if (re.Rows.Count > 0)
                                {
                                    var n1loop = new Loop_N1_856();
                                    hlMainLoop.N1Loop = new List<Loop_N1_856>();
                                    foreach (DataRow lrw in re.Rows)
                                    {

                                        var n = new N1();

                                        n.EntityIdentifierCode_01 = lrw["EntityIdentifierCode_01"] == null ? "" : lrw["EntityIdentifierCode_01"].ToString();
                                        n.Name_02 = lrw["Name_02"] == null ? "" : lrw["Name_02"].ToString();
                                        n.IdentificationCodeQualifier_03 = lrw["IdentificationCodeQualifier_03"] == null ? "" : lrw["IdentificationCodeQualifier_03"].ToString();
                                        n.IdentificationCode_04 = lrw["IdentificationCode_04"] == null ? "" : lrw["IdentificationCode_04"].ToString();
                                        n1loop.N1 = new N1();
                                        n1loop.N1 = n;
                                        hlMainLoop.N1Loop.Add(n1loop);


                                    }
                                }
                                result.HLLoop = new List<Loop_HL_856>();
                                result.HLLoop.Add(hlMainLoop);
                                var hlloop = new List<Loop_HL_856>();
                                d = new Database(DBEngineType.MsSql);
                                d.con_string = con_str;

                                //   HLMainID="+hlMain+" and
                                d.text = "select * from T856H_hl with(nolock) where  MainID=" + MainID + " ";
                                //  HL Main
                                DataTable rew = d.table;
                                // HL line
                                int j = 0;
                                int hlline = 1;
                                foreach (DataRow lrw in rew.Rows)
                                {
                                    var lhlloop = new Loop_HL_856();
                                    lhlloop.HL = new HL();
                                    // HL
                                    var hl = lhlloop.HL;
                                    hl.HierarchicalIDNumber_01 = lrw["HierarchicalIDNumber_01"] == null ? "" : lrw["HierarchicalIDNumber_01"].ToString();
                                    if (hl.HierarchicalIDNumber_01.Length < 1)
                                        hl.HierarchicalIDNumber_01 = hlline.ToString();
                                    hlline++;
                                    hl.HierarchicalParentIDNumber_02 = lrw["HierarchicalParentIDNumber_02"] == null ? "" : lrw["HierarchicalParentIDNumber_02"].ToString();
                                    hl.HierarchicalLevelCode_03 = lrw["HierarchicalLevelCode_03"] == null ? "" : lrw["HierarchicalLevelCode_03"].ToString();
                                    hl.HierarchicalChildCode_04 = lrw["HierarchicalChildCode_04"] == null ? "" : lrw["HierarchicalChildCode_04"].ToString();
                                    lhlloop.HL = hl;

                                    int lHLID = Convert.ToInt32(lrw["ID"]);


                                    //  PKG
                                    d = new Database(DBEngineType.MsSql);
                                    d.con_string = con_str;

                                    d.text = "select * from T856H_hl_pkg  with(nolock) where HLID=" + lHLID + "  and MainID=" + MainID;

                                    re = d.table;
                                    if (re.Rows.Count > 0)
                                        lhlloop.PKG = new List<PKG>();
                                    foreach (DataRow llrw in re.Rows)
                                    {

                                        var pkg = new PKG();

                                        pkg.ItemDescriptionType_01 = llrw["ItemDescriptionType_01"] == null ? "" : llrw["ItemDescriptionType_01"].ToString();
                                        pkg.PackagingCharacteristicCode_02 = llrw["PackagingCharacteristicCode_02"] == null ? "" : llrw["PackagingCharacteristicCode_02"].ToString();
                                        pkg.Description_05 = llrw["Description_05"] == null ? "" : llrw["Description_05"].ToString();

                                        lhlloop.PKG.Add(pkg);


                                    }

                                    //  TD1
                                    d = new Database(DBEngineType.MsSql);
                                    d.con_string = con_str;

                                    d.text = "select * from T856H_hl_td1  with(nolock) where HLID=" + lHLID + "  and MainID=" + MainID;

                                    re = d.table;
                                    if (re.Rows.Count > 0)
                                        lhlloop.TD1 = new List<TD1>();
                                    foreach (DataRow llrw in re.Rows)
                                    {

                                        var td1 = new TD1();

                                        td1.WeightQualifier_06 = llrw["WeightQualifier_06"] == null ? "" : llrw["WeightQualifier_06"].ToString();
                                        td1.Weight_07 = llrw["Weight_07"] == null ? "" : llrw["Weight_07"].ToString();
                                        td1.UnitorBasisforMeasurementCode_08 = llrw["UnitorBasisforMeasurementCode_08"] == null ? "" : llrw["UnitorBasisforMeasurementCode_08"].ToString();
                                        td1.Volume_09 = llrw["Volume_09"] == null ? "" : llrw["Volume_09"].ToString();
                                        td1.UnitorBasisforMeasurementCode_10 = llrw["UnitorBasisforMeasurementCode_10"] == null ? "" : llrw["UnitorBasisforMeasurementCode_10"].ToString();
                                        lhlloop.TD1.Add(td1);


                                    }
                                    //  REF
                                    d = new Database(DBEngineType.MsSql);
                                    d.con_string = con_str;

                                    d.text = "select * from T856H_hl_ref  with(nolock) where HLID=" + lHLID + "  and MainID=" + MainID;

                                    re = d.table;
                                    if (re.Rows.Count > 0)
                                        lhlloop.REF = new List<REF>();
                                    foreach (DataRow llrw in re.Rows)
                                    {

                                        var rf = new REF();

                                        rf.ReferenceIdentificationQualifier_01 = llrw["ReferenceIdentificationQualifier_01"] == null ? "" : llrw["ReferenceIdentificationQualifier_01"].ToString();
                                        rf.ReferenceIdentification_02 = llrw["ReferenceIdentification_02"] == null ? "" : llrw["ReferenceIdentification_02"].ToString();
                                        lhlloop.REF.Add(rf);


                                    }

                                    d = new Database(DBEngineType.MsSql);
                                    d.con_string = con_str;

                                    d.text = "select * from T856H_hl_lin with(nolock) where HLID=" + lHLID + " and  MainID=" + MainID;

                                    re = d.table;
                                    //LN
                                    if (re.Rows.Count > 0)
                                        lhlloop.LIN = new LIN();
                                    foreach (DataRow linrow in re.Rows)
                                    {
                                        lhlloop.LIN.AssignedIdentification_01 = (linrow["hAssignedIdentification_01"] == null ? "" : linrow["hAssignedIdentification_01"].ToString());
                                        lhlloop.LIN.ProductServiceIDQualifier_02 = (linrow["hProductServiceIDQualifier_02"] == null ? "" : linrow["hProductServiceIDQualifier_02"].ToString());
                                        lhlloop.LIN.ProductServiceID_03 = (linrow["hProductServiceID_03"] == null ? "" : linrow["hProductServiceID_03"].ToString());
                                        lhlloop.LIN.ProductServiceIDQualifier_04 = (linrow["hProductServiceIDQualifier_04"] == null ? "" : linrow["hProductServiceIDQualifier_04"].ToString());
                                        lhlloop.LIN.ProductServiceID_05 = (linrow["hProductServiceID_05"] == null ? "" : linrow["hProductServiceID_05"].ToString());
                                        lhlloop.LIN.ProductServiceIDQualifier_06 = (linrow["hProductServiceIDQualifier_06"] == null ? "" : linrow["hProductServiceIDQualifier_06"].ToString());
                                        lhlloop.LIN.ProductServiceID_07 = (linrow["hProductServiceID_07"] == null ? "" : linrow["hProductServiceID_07"].ToString());
                                        lhlloop.LIN.ProductServiceIDQualifier_08 = (linrow["hProductServiceIDQualifier_08"] == null ? "" : linrow["hProductServiceIDQualifier_08"].ToString());
                                        lhlloop.LIN.ProductServiceID_09 = (linrow["hProductServiceID_09"] == null ? "" : linrow["hProductServiceID_09"].ToString());
                                        lhlloop.LIN.ProductServiceIDQualifier_10 = (linrow["hProductServiceIDQualifier_10"] == null ? "" : linrow["hProductServiceIDQualifier_10"].ToString());
                                        lhlloop.LIN.ProductServiceID_11 = (linrow["hProductServiceID_11"] == null ? "" : linrow["hProductServiceID_11"].ToString());



                                    }
                                    d = new Database(DBEngineType.MsSql);
                                    d.con_string = con_str;

                                    d.text = "select * from T856H_hl_sn1 with(nolock) where HLID=" + lHLID + " and  MainID=" + MainID;

                                    re = d.table;

                                    // SN1
                                    if (re.Rows.Count > 0)
                                        lhlloop.SN1 = new SN1();
                                    foreach (DataRow linrow in re.Rows)
                                    {
                                        lhlloop.SN1.AssignedIdentification_01 = (linrow["AssignedIdentification_01"] == null ? "" : linrow["AssignedIdentification_01"]).ToString();
                                        lhlloop.SN1.NumberofUnitsShipped_02 = (linrow["NumberofUnitsShipped_02"] == null ? "" : linrow["NumberofUnitsShipped_02"]).ToString();
                                        lhlloop.SN1.UnitorBasisforMeasurementCode_03 = (linrow["UnitorBasisforMeasurementCode_03"] == null ? "" : linrow["UnitorBasisforMeasurementCode_03"]).ToString();
                                    }


                                    result.HLLoop.Add(lhlloop);

                                }

                            }

                            if (result.HasErrors)
                            {
                                //Error Log
                                d = new Database(DBEngineType.MsSql);
                                d.con_string = con_str;

                                //  Indicates the start of a purchase order transaction set and assigns a control number.
                                d.text = "insert into TLogs (MainID,Tablo, Mesaj) values(" + MainID + ",'T846H','" + result.ErrorContext.ToString() + "')";
                                d.exec_non();

                            }
                            else
                            {
                                //Update DB and flag

                                writer.Write(result);
                                writer.Flush();
                                writer.Dispose();

                                string str = File.ReadAllText(fileName);
                                if (str.IndexOf("~") > 10)
                                {
                                    str = str.Replace("~", "~\r\n");
                                    //str = str.Replace("*U*", "*^*");
                                    //str = str.Replace("GS*IN*", "GS*RE*");
                                    File.WriteAllText(fileName, str);
                                }
                                d = new Database(DBEngineType.MsSql);
                                d.con_string = con_str;

                                //  Indicates the start of a purchase order transaction set and assigns a control number.
                                d.text = "update T856H  set Updated=1  where  ID=" + MainID;
                                d.exec_non();
                            }


                        }


                    }
                    catch (Exception x)
                    {
                        LoggerToFile("Rollback format is -- " + format + " -- " + DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"));
                        //Error Log
                        d = new Database(DBEngineType.MsSql);
                        d.con_string = con_str;

                        //  Indicates the start of a purchase order transaction set and assigns a control number.
                        d.text = "insert into TLogs (MainID,Tablo, Mesaj) values(" + MainID + ",'T846H','" + x.ToString() + "')";
                        d.exec_non();
                    }


                }

            }

            return MainID;
        }

        private void EDI846(string sOutboundFolder, out Database d, out string format, out int MainID)
        {
            MainID = 0;
            {






                format = "EDI 846";

                d = new Database(DBEngineType.MsSql);
                d.con_string = con_str;

                //  Indicates the start of a purchase order transaction set and assigns a control number.
                d.text = "select * from T846H   with(nolock) where   Updated=0 and In2Out=1 and IsOk=1";

                DataTable dt = d.table;
                foreach (DataRow r in dt.Rows)
                {

                    try
                    {
                        var result = new TS846();
                        X12WriterSettings xs = new X12WriterSettings();
                        xs.PreserveWhitespace = true;
                        xs.AutoTrailers = true;
                        xs.Encoding = Encoding.ASCII;
                        xs.Separators.Segment = Separators.X12.Segment;
                        Database dnm = new Database(DBEngineType.MsSql);
                        dnm.con_string = con_str;

                        //  Indicates the start of a purchase order transaction set and assigns a control number.
                        dnm.text = "select top 1 Name_02 from T846H_n1  with(nolock) where  MainID=" + r["ID"].ToString();
                        //  REF
                        DataTable rnm = dnm.table;
                        string n1 = rnm.Rows[0][0].ToString();
                        String fileName = sOutboundFolder + @"\3PL_" + n1 + "_846_" + r["ReferenceIdentification_03"] + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".txt";
                        using (var writer = new X12Writer(fileName, false, xs))

                        {


                            var isa = new ISA();
                            isa.InterchangeControlVersionNumber_12 = "00402";
                            isa.InterchangeControlNumber_13 = (Convert.ToInt32(r["ID"]) + 100000000).ToString();
                            isa.AcknowledgementRequested_14 = "0";
                            isa.SecurityInformationQualifier_3 = "00";
                            isa.SecurityInformation_4 = "          ";
                            isa.AuthorizationInformation_2 = "          ";
                            isa.SenderIDQualifier_5 = "ZZ";
                            isa.InterchangeSenderID_6 = "SSETRPRD       ";
                            isa.ReceiverIDQualifier_7 = "ZZ";
                            isa.InterchangeReceiverID_8 = "SCHE3PLTR      ";
                            isa.InterchangeDate_9 = DateTime.Now.ToString("yyMMdd");
                            isa.InterchangeTime_10 = DateTime.Now.ToString("HHmm");
                            isa.UsageIndicator_15 = "P";
                            isa.AuthorizationInformationQualifier_1 = "00";
                            isa.InterchangeControlStandardsIdentifier_11 = "^";
                            //writer.Write(SegmentBuilders.BuildIsa(r["TransactionSetPurposeCode_01"].ToString(), "SSETRPRD ", "ZZ", "SCHE3PLTR  ", "ZZ", "0", "P"));
                            writer.Write(isa);
                            //writer.Write(SegmentBuilders.BuildGs(r["TransactionSetPurposeCode_01"].ToString(), "SSETRPRD ", "SCHE3PLTR  "));
                            var gs = new GS();
                            gs.CodeIdentifyingInformationType_1 = "RE";
                            gs.SenderIDCode_2 = "SSETRPRD ";
                            gs.ReceiverIDCode_3 = "SCHE3PLTR  ";
                            gs.Date_4 = DateTime.Now.ToString("yyyyMMdd");
                            gs.Time_5 = DateTime.Now.ToString("HHmm");
                            gs.GroupControlNumber_6 = (Convert.ToInt32(r["ID"]) + 100000000).ToString();
                            gs.TransactionTypeCode_7 = "X";
                            gs.VersionAndRelease_8 = "004010";

                            writer.Write(gs);

                            result.BIA = new BIA();
                            result.BIA.TransactionSetPurposeCode_01 = (r["TransactionSetPurposeCode_01"] == null ? "" : r["TransactionSetPurposeCode_01"].ToString());
                            result.BIA.ReportTypeCode_02 = (r["ReportTypeCode_02"] == null ? "" : r["ReportTypeCode_02"].ToString());
                            result.BIA.ReferenceIdentification_03 = (r["ReferenceIdentification_03"] == null ? "" : r["ReferenceIdentification_03"].ToString());
                            result.BIA.Date_04 = r["Date_04"].ToString();
                            result.BIA.Time_05 = r["Time_05"].ToString();

                            result.ST = new ST();
                            result.ST.TransactionSetIdentifierCode_01 = "846";
                            result.ST.TransactionSetControlNumber_02 = "000000001";


                            MainID = Convert.ToInt32(r["ID"]);


                            d = new Database(DBEngineType.MsSql);
                            d.con_string = con_str;

                            //  Indicates the start of a purchase order transaction set and assigns a control number.
                            d.text = "select * from T846H_ref  with(nolock) where  MainID=" + MainID;
                            //  REF
                            DataTable re = d.table;
                            result.REF = new List<REF>();
                            foreach (DataRow rw in re.Rows)
                            {

                                var rf = new REF();
                                d = new Database(DBEngineType.MsSql);
                                d.con_string = con_str;
                                d.cmd_type = CommandType.StoredProcedure;

                                rf.ReferenceIdentificationQualifier_01 = (rw["ReferenceIdentificationQualifier_01"] == null ? "" :
                                    rw["ReferenceIdentificationQualifier_01"].ToString());
                                rf.ReferenceIdentification_02 = (rw["ReferenceIdentification_02"] == null ? "" : rw["ReferenceIdentification_02"].ToString());
                                result.REF.Add(rf);
                            }


                            //  N1 N2
                            d = new Database(DBEngineType.MsSql);
                            d.con_string = con_str;

                            d.text = "select * from T846H_n1  with(nolock) where  MainID=" + MainID;
                            //  REF
                            re = d.table;
                            if (re.Rows.Count > 0)
                            {
                                result.N1Loop = new List<Loop_N1_846>();
                                var n1loop = new Loop_N1_846();
                                foreach (DataRow rw in re.Rows)
                                {

                                    var n = new N1();

                                    n.EntityIdentifierCode_01 = rw["EntityIdentifierCode_01"] == null ? "" : rw["EntityIdentifierCode_01"].ToString();
                                    n.Name_02 = rw["Name_02"] == null ? "" : rw["Name_02"].ToString();
                                    n.IdentificationCodeQualifier_03 = rw["IdentificationCodeQualifier_03"] == null ? "" : rw["IdentificationCodeQualifier_03"].ToString();
                                    n.IdentificationCode_04 = rw["IdentificationCode_04"] == null ? "" : rw["IdentificationCode_04"].ToString();
                                    n1loop.N1 = new N1();
                                    n1loop.N1 = n;
                                    result.N1Loop.Add(n1loop);


                                }
                            }

                            //  N1 N2
                            d = new Database(DBEngineType.MsSql);
                            d.con_string = con_str;

                            d.text = "select * from T846H_lin  with(nolock) where  MainID=" + MainID;
                            //      PO1
                            re = d.table;
                            result.LINLoop = new List<Loop_LIN_846>();
                            int kl = 0;
                            foreach (DataRow rw in re.Rows)
                            {

                                // PO PO1
                                d = new Database(DBEngineType.MsSql);
                                d.con_string = con_str;
                                d.cmd_type = CommandType.StoredProcedure;
                                var lin = new Loop_LIN_846();
                                lin.LIN = new LIN();
                                lin.LIN.ProductServiceIDQualifier_02 = (rw["ProductServiceIDQualifier_02"] == null ? "" : rw["ProductServiceIDQualifier_02"].ToString());
                                lin.LIN.ProductServiceID_03 = rw["ProductServiceID_03"] == null ? "" : rw["ProductServiceID_03"].ToString();
                                lin.LIN.ProductServiceIDQualifier_04 = rw["ProductServiceIDQualifier_04"] == null ? "" : rw["ProductServiceIDQualifier_04"].ToString();
                                lin.LIN.ProductServiceID_05 = rw["ProductServiceID_05"] == null ? "" : rw["ProductServiceID_05"].ToString();
                                lin.LIN.ProductServiceIDQualifier_06 = rw["ProductServiceIDQualifier_06"] == null ? "" : rw["ProductServiceIDQualifier_06"].ToString();
                                lin.LIN.ProductServiceID_07 = rw["ProductServiceID_07"] == null ? "" : rw["ProductServiceID_07"].ToString();

                                int LinID = Convert.ToInt32(rw["ID"]);
                                result.LINLoop.Add(lin);
                                // QTY
                                d = new Database(DBEngineType.MsSql);
                                d.con_string = con_str;

                                d.text = "select * from T846H_lin_qty  with(nolock) where  MainID=" + MainID + " and LINID=" + LinID + " order by _index asc";
                                //      PO1
                                DataTable rws = d.table; lin.QTYLoop = new List<Loop_QTY_846>();
                                int s = 0;
                                foreach (DataRow qty in rws.Rows)
                                {

                                    var qt = new Loop_QTY_846();
                                    qt.QTY = new QTY();
                                    qt.QTY.QuantityQualifier_01 = qty["QuantityQualifier_01"].ToString();
                                    qt.QTY.Quantity_02 = qty["Quantity_02"].ToString();
                                    int qtyid = Convert.ToInt32(qty["ID"]);
                                    d = new Database(DBEngineType.MsSql);
                                    d.con_string = con_str;

                                    d.text = "select * from T846H_lin_uit  with(nolock) where QTYID=" + qtyid + "  and MainID=" + MainID + " and LINID=" + LinID + " order by _index asc";
                                    //      PO1

                                    rws = d.table;
                                    foreach (DataRow rl in rws.Rows)
                                    {
                                        qt.UIT = new List<UIT>();
                                        var uit = new UIT();
                                        //   result.LINLoop[kl].QTYLoop[s].UIT = new List<UIT>();
                                        uit.CompositeUnitofMeasure_01 = new C001();
                                        uit.CompositeUnitofMeasure_01.UnitorBasisforMeasurementCode_01 = rl["UnitorBasisforMeasurementCode_01"] == null ? "" : rl["UnitorBasisforMeasurementCode_01"].ToString();
                                        qt.UIT.Add(uit);
                                        // result.LINLoop[kl].QTYLoop[s].UIT.Add(uit);
                                    }
                                    lin.QTYLoop.Add(qt);
                                    result.LINLoop.Add(lin);
                                    s++;
                                }

                                kl++;

                            }

                            if (result.HasErrors)
                            {
                                LoggerToFile("Rollback format is -- " + format + " -- " + DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"));
                                //Error Log
                                d = new Database(DBEngineType.MsSql);
                                d.con_string = con_str;

                                //  Indicates the start of a purchase order transaction set and assigns a control number.
                                d.text = "insert into TLogs (MainID,Tablo, Mesaj) values(" + MainID + ",'T846H','" + result.ErrorContext.ToString() + "')";
                                d.exec_non();

                            }
                            else
                            {
                                //Update DB and flag

                                writer.Write(result);
                                writer.Flush();
                                writer.Dispose();
                                string str = File.ReadAllText(fileName);
                                if (str.IndexOf("~") > 10)
                                {
                                    str = str.Replace("~", "~\r\n");
                                    //str = str.Replace("*U*", "*^*");
                                    //str = str.Replace("GS*IN*", "GS*RE*");
                                    str = str.Replace("*00204*", "*00402*");
                                    File.WriteAllText(fileName, str);
                                }
                                d = new Database(DBEngineType.MsSql);
                                d.con_string = con_str;

                                //  Indicates the start of a purchase order transaction set and assigns a control number.
                                d.text = "update T846H  set Updated=1  where  ID=" + MainID;
                                d.exec_non();
                            }



                        }


                    }
                    catch (Exception x)
                    {
                        LoggerToFile("Rollback format is -- " + format + " -- " + DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"));
                        //Error Log
                        d = new Database(DBEngineType.MsSql);
                        d.con_string = con_str;

                        //  Indicates the start of a purchase order transaction set and assigns a control number.
                        d.text = "insert into TLogs (MainID,Tablo, Mesaj) values(" + MainID + ",'T846H','" + x.ToString() + "')";
                        d.exec_non();

                    }


                }

            }
        }

        private void WriteIContent(ref Database d, int MainID, TS856 result, ref DataTable re, ref int hlline, string ParentNo, int lHLID2)
        {
            d.text = "select * from T856H_hl with(nolock) where  MainID=" + MainID + " and HierarchicalLevelCode_03 = 'I' and HierarchicalParentIDNumber_02 = '" + ParentNo + "'";
            DataTable rewI = d.table;
            foreach (DataRow lrwI in rewI.Rows)
            {
                hlline = WriteContent(out d, MainID, result, out re, hlline, lrwI);
            }
            d.text = "select * from T856H_hl with(nolock) where  MainID=" + MainID + " and HierarchicalLevelCode_03 = 'I' and HLID = " + lHLID2;
            DataTable rewIwithoutPallet = d.table;
            foreach (DataRow lrwI in rewIwithoutPallet.Rows)
            {
                hlline = WriteContent(out d, MainID, result, out re, hlline, lrwI);
            }
        }

        private int WriteContent(out Database d, int MainID, TS856 result, out DataTable re, int hlline, DataRow lrw)
        {
            var lhlloop = new Loop_HL_856();
            lhlloop.HL = new HL();
            // HL
            var hl = lhlloop.HL;
            hl.HierarchicalIDNumber_01 = lrw["HierarchicalIDNumber_01"] == null ? "" : lrw["HierarchicalIDNumber_01"].ToString();
            if (hl.HierarchicalIDNumber_01.Length < 1)
                hl.HierarchicalIDNumber_01 = hlline.ToString();
            hlline++;
            hl.HierarchicalParentIDNumber_02 = lrw["HierarchicalParentIDNumber_02"] == null ? "" : lrw["HierarchicalParentIDNumber_02"].ToString();
            hl.HierarchicalLevelCode_03 = lrw["HierarchicalLevelCode_03"] == null ? "" : lrw["HierarchicalLevelCode_03"].ToString();
            hl.HierarchicalChildCode_04 = lrw["HierarchicalChildCode_04"] == null ? "" : lrw["HierarchicalChildCode_04"].ToString();
            lhlloop.HL = hl;

            int lHLID = Convert.ToInt32(lrw["ID"]);

            //  PKG
            d = new Database(DBEngineType.MsSql);
            d.con_string = con_str;

            d.text = "select * from T856H_hl_pkg  with(nolock) where HLID=" + lHLID + "  and MainID=" + MainID;

            re = d.table;
            if (re.Rows.Count > 0)
                lhlloop.PKG = new List<PKG>();
            foreach (DataRow llrw in re.Rows)
            {

                var pkg = new PKG();

                pkg.ItemDescriptionType_01 = llrw["ItemDescriptionType_01"] == null ? "" : llrw["ItemDescriptionType_01"].ToString();
                pkg.PackagingCharacteristicCode_02 = llrw["PackagingCharacteristicCode_02"] == null ? "" : llrw["PackagingCharacteristicCode_02"].ToString();
                pkg.Description_05 = llrw["Description_05"] == null ? "" : llrw["Description_05"].ToString();

                lhlloop.PKG.Add(pkg);


            }

            //  TD1
            d = new Database(DBEngineType.MsSql);
            d.con_string = con_str;

            d.text = "select * from T856H_hl_td1  with(nolock) where HLID=" + lHLID + "  and MainID=" + MainID;

            re = d.table;
            if (re.Rows.Count > 0)
                lhlloop.TD1 = new List<TD1>();
            foreach (DataRow llrw in re.Rows)
            {

                var td1 = new TD1();

                td1.WeightQualifier_06 = llrw["WeightQualifier_06"] == null ? "" : llrw["WeightQualifier_06"].ToString();
                td1.Weight_07 = llrw["Weight_07"] == null ? "" : llrw["Weight_07"].ToString();
                td1.UnitorBasisforMeasurementCode_08 = llrw["UnitorBasisforMeasurementCode_08"] == null ? "" : llrw["UnitorBasisforMeasurementCode_08"].ToString();
                td1.Volume_09 = llrw["Volume_09"] == null ? "" : llrw["Volume_09"].ToString();
                td1.UnitorBasisforMeasurementCode_10 = llrw["UnitorBasisforMeasurementCode_10"] == null ? "" : llrw["UnitorBasisforMeasurementCode_10"].ToString();
                lhlloop.TD1.Add(td1);


            }
            //  REF
            d = new Database(DBEngineType.MsSql);
            d.con_string = con_str;

            d.text = "select * from T856H_hl_ref  with(nolock) where HLID=" + lHLID + "  and MainID=" + MainID;

            re = d.table;
            if (re.Rows.Count > 0)
                lhlloop.REF = new List<REF>();
            foreach (DataRow llrw in re.Rows)
            {

                var rf = new REF();

                rf.ReferenceIdentificationQualifier_01 = llrw["ReferenceIdentificationQualifier_01"] == null ? "" : llrw["ReferenceIdentificationQualifier_01"].ToString();
                rf.ReferenceIdentification_02 = llrw["ReferenceIdentification_02"] == null ? "" : llrw["ReferenceIdentification_02"].ToString();
                lhlloop.REF.Add(rf);


            }

            d = new Database(DBEngineType.MsSql);
            d.con_string = con_str;

            d.text = "select * from T856H_hl_lin with(nolock) where HLID=" + lHLID + " and  MainID=" + MainID;

            re = d.table;
            //LN
            if (re.Rows.Count > 0)
                lhlloop.LIN = new LIN();
            foreach (DataRow linrow in re.Rows)
            {
                lhlloop.LIN.AssignedIdentification_01 = (linrow["hAssignedIdentification_01"] == null ? "" : linrow["hAssignedIdentification_01"].ToString());
                lhlloop.LIN.ProductServiceIDQualifier_02 = (linrow["hProductServiceIDQualifier_02"] == null ? "" : linrow["hProductServiceIDQualifier_02"].ToString());
                lhlloop.LIN.ProductServiceID_03 = (linrow["hProductServiceID_03"] == null ? "" : linrow["hProductServiceID_03"].ToString());

                if (linrow["hProductServiceIDQualifier_04"] != null)
                    if (linrow["hProductServiceIDQualifier_04"].ToString().Length > 1)

                        lhlloop.LIN.ProductServiceIDQualifier_04 = (linrow["hProductServiceIDQualifier_04"] == null ? "" : linrow["hProductServiceIDQualifier_04"].ToString());

                if (linrow["hProductServiceID_05"] != null)
                    if (linrow["hProductServiceID_05"].ToString().Length > 1)

                        lhlloop.LIN.ProductServiceID_05 = (linrow["hProductServiceID_05"] == null ? "" : linrow["hProductServiceID_05"].ToString());

                if (linrow["hProductServiceIDQualifier_06"] != null)
                    if (linrow["hProductServiceIDQualifier_06"].ToString().Length > 0)

                        lhlloop.LIN.ProductServiceIDQualifier_06 = (linrow["hProductServiceIDQualifier_06"] == null ? "" : linrow["hProductServiceIDQualifier_06"].ToString());

                if (linrow["hProductServiceID_07"] != null)
                    if (linrow["hProductServiceID_07"].ToString().Length > 0)

                        lhlloop.LIN.ProductServiceID_07 = (linrow["hProductServiceID_07"] == null ? "" : linrow["hProductServiceID_07"].ToString());

                if (linrow["hProductServiceIDQualifier_08"] != null)
                    if (linrow["hProductServiceIDQualifier_08"].ToString().Length > 0)


                        lhlloop.LIN.ProductServiceIDQualifier_08 = (linrow["hProductServiceIDQualifier_08"] == null ? "" : linrow["hProductServiceIDQualifier_08"].ToString());

                if (linrow["hProductServiceID_09"] != null)
                    if (linrow["hProductServiceID_09"].ToString().Length > 0)

                        lhlloop.LIN.ProductServiceID_09 = (linrow["hProductServiceID_09"] == null ? "" : linrow["hProductServiceID_09"].ToString());


                if (linrow["hProductServiceIDQualifier_10"] != null)
                    if (linrow["hProductServiceIDQualifier_10"].ToString().Length > 0)


                        lhlloop.LIN.ProductServiceIDQualifier_10 = (linrow["hProductServiceIDQualifier_10"] == null ? "" : linrow["hProductServiceIDQualifier_10"].ToString());

                if (linrow["hProductServiceID_11"] != null)
                    if (linrow["hProductServiceID_11"].ToString().Length > 0)


                        lhlloop.LIN.ProductServiceID_11 = (linrow["hProductServiceID_11"] == null ? "" : linrow["hProductServiceID_11"].ToString());



            }
            d = new Database(DBEngineType.MsSql);
            d.con_string = con_str;

            d.text = "select * from T856H_hl_sn1 with(nolock) where HLID=" + lHLID + " and  MainID=" + MainID;

            re = d.table;

            // SN1
            if (re.Rows.Count > 0)
                lhlloop.SN1 = new SN1();
            foreach (DataRow linrow in re.Rows)
            {
                lhlloop.SN1.AssignedIdentification_01 = (linrow["AssignedIdentification_01"] == null ? "" : linrow["AssignedIdentification_01"]).ToString();
                lhlloop.SN1.NumberofUnitsShipped_02 = (linrow["NumberofUnitsShipped_02"] == null ? "" : linrow["NumberofUnitsShipped_02"]).ToString();
                lhlloop.SN1.UnitorBasisforMeasurementCode_03 = (linrow["UnitorBasisforMeasurementCode_03"] == null ? "" : linrow["UnitorBasisforMeasurementCode_03"]).ToString();
            }


            result.HLLoop.Add(lhlloop);
            return hlline;
        }

        private void LoggerToFile(string Message)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + @"\Logs";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\ServiceLog_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";

            if (!File.Exists(filepath))
            {
                using (StreamWriter writer = File.CreateText(filepath))
                {
                    writer.Write(Message);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
        }
    }
}
